<?php
namespace App\Core;

use App\Document\Utils;

class XmlGroupParser
{
    /**
     * from the root project dir, with first slash
     */
    const PATH_TO_OFFERS_FILE = '/_offers/offers.xml';

    /**
     * @return \SimpleXMLElement
     */
    public function rawXml()
    {
        global $kernel;
        $xml =  simplexml_load_file($kernel->getProjectDir() . self::PATH_TO_OFFERS_FILE);
        return $xml;
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return array
     */
    public function getOffers(\SimpleXMLElement $xml)
    {
        $out = [];
        $offers = json_decode(json_encode($xml->ПакетПредложений->Предложения), 1);
        foreach ($offers as $offer) {
            $out[] = $offer;
        }
        return $out[0];
    }

    /**
     * @param array $offer
     * @return array
     */
    public function parseOffer(array $offer)
    {
        $out = [];
        $out['productId'] = $offer['Ид'];
        $out['name'] = $offer['Наименование'];
        $out['item'] = $offer['БазоваяЕдиница'];
        $out['price'] = intval($offer['Цены']['Цена']['ЦенаЗаЕдиницу']);
        if (isset($offer, $offer['Количество'])) {
            $out['count'] = intval($offer['Количество']);
        }
        else {
            $out['count'] = null;
        }
        return $out;
    }

}

