<?php
namespace App\Core;

use App\Document\Utils;

class XmlParser
{
    /**
     * from the root project dir, with first slash
     */
    const PATH_TO_OFFERS_FILE = '/_offers/offers.xml';

    /**
     * @return \SimpleXMLElement
     */
    public function rawXml()
    {
        global $kernel;
        $xml =  simplexml_load_file($kernel->getProjectDir() . self::PATH_TO_OFFERS_FILE);
        return $xml;
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return array
     */
    public function getOffers(\SimpleXMLElement $xml)
    {
        $out = [];
        $offers = json_decode(json_encode($xml->ПакетПредложений->Предложения), 1);
        foreach ($offers as $offer) {
            $out[] = $offer;
        }
        return $out[0];
    }

    /**
     * @param array $offer
     * @return array
     */
    public function parseOffer(array $offer)
    {
        $out = [];
        $out['productId'] = $offer['Ид'];
        $out['name'] = $offer['Наименование'];
        $out['item'] = $offer['БазоваяЕдиница'];
        $out['price'] = intval($offer['Цены']['Цена']['ЦенаЗаЕдиницу']);
        if (isset($offer, $offer['Количество'])) {
            $out['count'] = intval($offer['Количество']);
        }
        else {
            $out['count'] = null;
        }
        return $out;
    }

    private $out = [];

    /**
     * парсится часть файла 1С, содержащая только группы, gr.xml
    $file = '/var/www/obt.nottes.net/_offers/import.xml';
    $xml = simplexml_load_file($file);
    $res = json_decode(json_encode($xml), 1);
    $groups = $this->parseGroup(0, null, $res);

     * @param $level
     * @param $parentGroupId
     * @param $group
     * @return array
     */
    public function parseGroup($level, $parentGroupId, $group)
    {
        $group = $group['Группа'];
        if (isset($group['Ид'])) {
            $this->out[] = [
                'categoryId' => $group['Ид'],
                'categoryName' => $group['Наименование'],
                'categoryParent' => $parentGroupId,
                'categoryLevel' => $level,
                'categoryFileName' => ''
            ];
        }
        else {
            foreach ($group as $key => $value) {
                $this->out[] = [
                    'categoryId' => $value['Ид'],
                    'categoryName' => $value['Наименование'],
                    'categoryParent' => $parentGroupId,
                    'categoryLevel' => $level,
                    'categoryFileName' => '',
                ];
                if (isset($value['Группы'])) {
                    $this->parseGroup($level+1, $value['Ид'], $value['Группы']);
                }
            }
        }

        return $this->out;
    }


}

