<?php
namespace App\Controller;
use App\Document\Items\AddField;
use App\Document\Items\Category;
use App\Document\Items\Good;
use App\Document\Items\Page;
use App\Document\Items\Product;
use App\Document\MongoManager;
use App\Document\MongoManager3;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminProductController extends  Controller
{
    use AllControllerTrait;

    /**
     * @Route("/admin/product/list")
     * @param Request $request
     * @param Product $product
     * @param Category $category
     * @return Response
     */
/*    public function productList(Request $request, Product $product, Category $category)
    {
        AdminAuthController::checkCookie($request);
        $page = $request->get('page')?intval($request->get('page')):1;
        $sort = $request->get('sort');
        $order = $request->get('order')?$request->get('order'):'asc';
        $antiOrder = ($order == 'asc')?'desc':'asc';
        $products = $product->getProductsPage($page, $sort, $order);
        $productsCount = $product->getProductsCount();
        $pageCount = intval($productsCount/50);
        $content = $this->renderView('admin/product/list.html.twig', [
            'products' => $products,
            'anti_order' => $antiOrder,
            'page_minus' => ($page-1)>0?($page-1):'#',
            'page_plus' => (($page+1)<$pageCount)?($page+1):'#',
            'page' => $page,
            'sort' => $sort,
            'order' => $order,
        ]);

        $tree = $category->getTree();
        $treeContentModal = $this->showCat($tree);
        $treeContentModal = $this->renderView('admin/category/treeContentModal.html.twig', [
            'treeContent' => $treeContentModal,
        ]);
        $content .= $treeContentModal;


        $html = $this->renderAdminPage('Товары', $content,
            ['https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js',
                '/js/admin/product/list.js'
            ],
            [
                'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css',
            ]

        );
        return new Response($html);
    }*/

    /**
     * @Route("/admin/goodsByCategory/{categoryId}")
     * @param Request $request
     * @param Good $good
     * @param Category $category
     * @return Response
     */
    public function productList2(Request $request, Good $good, Category $category, $categoryId)
    {
        AdminAuthController::checkCookie($request);
        $page = $request->get('page')?intval($request->get('page')):1;
        $sort = $request->get('sort');
        $order = $request->get('order')?$request->get('order'):'asc';
        $antiOrder = ($order == 'asc')?'desc':'asc';
        $categoryName = $category->getCategoryByCategoryId($categoryId)['categoryName'];
        $products = $good->getGoodsPage($categoryId, $page, $sort, $order);
        $productsCount = $good->getGoodsCount();
        $pageCount = intval($productsCount/50);
        $content = $this->renderView('admin/product/list.html.twig', [
            'products' => $products,
            'anti_order' => $antiOrder,
            'page_minus' => ($page-1)>0?($page-1):'#',
            'page_plus' => (($page+1)<$pageCount)?($page+1):'#',
            'page' => $page,
            'sort' => $sort,
            'order' => $order,
            'categoryName' => $categoryName
        ]);

        $tree = $category->getTree();
        $treeContentModal = $this->showCat($tree);
        $treeContentModal = $this->renderView('admin/category/treeContentModal.html.twig', [
            'treeContent' => $treeContentModal,
        ]);
        $content .= $treeContentModal;


        $html = $this->renderAdminPage('Товары', $content,
            ['https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js',
                '/js/admin/product/list.js'
            ],
            [
                'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css',
            ]

        );
        return new Response($html);
    }

    /**
     * search page
     * @Route("/admin/product/search")
     * @param Request $request
     * @param Product $product
     * @param Category $category
     * @return Response
     * @throws \Exception
     */
    public function productSearch(Request $request, Good $good, Category $category)
    {
        AdminAuthController::checkCookie($request);
        $searchString = trim(urldecode($request->get('search')));
        $products = $good->searchGoodByRegEx($searchString);
        $out = [];
        foreach ($products as $product) {
            $goodCategory = $category->getCategoryByCategoryId($product['group']);
            $product['categoryName'] = $goodCategory['categoryName'];
            $product['categoryId'] = $goodCategory['categoryId'];
            $out[] = $product;
        }
        $content = $this->renderView('/admin/product/search.html.twig', ['products' => $out]);

        $tree = $category->getTree();
        $treeContentModal = $this->showCat($tree);
        $treeContentModal = $this->renderView('admin/category/treeContentModal.html.twig', [
            'treeContent' => $treeContentModal,
        ]);
        $content .= $treeContentModal;

        $html = $this->renderAdminPage('Поиск по запросу: <b>' . $searchString . '</b>', $content,
            ['https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js',
                '/js/admin/product/list.js'
            ],
            [
                'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css',
            ]
        );
        return new Response($html);
    }

    /**
     * edit product page
     * @Route("/admin/product/edit/{goodId}")
     * @param Request $request
     * @param Product $product
     * @param Category $category
     * @return Response
     */
    public function productEdit(Request $request, Good $good, Category $category, AddField $addField, $goodId)
    {
        AdminAuthController::checkCookie($request);
        $editedProduct = $good->getGoogBygoodId($goodId);
/*        if ($editedProduct['files']) {
            $files = json_decode($editedProduct['files'], 1);
        }
        else {
            $files = [false];
        }*/
        $content = $this->renderView('admin/product/edit.html.twig', [
            'product' => $editedProduct,
            'files' => [],
            'fields' => $addField->getFieldsByProductId($goodId)
        ]);

        $tree = $category->getTree();
        $treeContentModal = $this->showCat($tree);
        $treeContentModal = $this->renderView('admin/category/treeContentModal.html.twig', [
            'treeContent' => $treeContentModal,
        ]);
        $content .= $treeContentModal;

        $html = $this->renderAdminPage('Редактирование товара: <span style="color:#2dbbff;">' .
            $editedProduct['name'] . '</span>', $content,
            [
                'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js',
                '/js/admin/product/init.js'
            ],
            [
                'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css',
            ]
        );
        return new Response($html);
    }

    /**
     * ajax upload image
     * @Route("/admin/product/upload")
     * @param Request $request
     * @return Response
     */
    public function uploadProductFile(Request $request)
    {
        global $kernel;
        $status = [
            'result' => 'error',
        ];
        $rootDir = $kernel->getProjectDir();
        $uploadPath = $rootDir . '/' . $this->getParameter('app.product_upload_path');
        $file = $request->files->get('categoryFile');
        if(!is_null($file) && preg_match('/image/', $file->getMimeType())){
            $filename = uniqid().".".$file->getClientOriginalExtension();
            $file->move($uploadPath, $filename);
        }
        else {
            return new Response(json_encode($status));
        }
        $status = [
            'result' => 'ok',
            'file' => $this->getParameter('app.product_upload_url') . $filename,
        ];
        return new Response(json_encode($status));
    }

    /**
     * save product by ajax
     * @Route("/admin/product/save")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function saveProduct(Request $request, Product $product)
    {
        $productId = $request->get('productId');
        $name = $request->get('name');
        $category = $request->get('category');
        $rootCategory = $request->get('rootCategory');
        $price = $request->get('price');
        $description = $request->get('description');
        $files = $request->get('files');
        $builder = $product->manager->createQueryBuilder(Product::class);
        $builder
            ->updateOne()
            ->field('productId')->equals($productId)
            ->field('name')->set($name)
            ->field('category')->set($category)
            ->field('rootCategory')->set($rootCategory)
            ->field('price')->set($price)
            ->field('description')->set($description)
            ->field('files')->set($files);
        try {
            $builder
                ->getQuery()
                ->execute();
        }
        catch(\Exception $exception) {
            echo $exception->getMessage();
        }
        return new Response('ok');
    }

    /**
     * ajax
     * @Route("/admin/product/addField")
     * @param Request $request
     * @return Response
     */
    public function addField(Request $request)
    {
        $html = $this->renderView('admin/product/addField.html.twig');
        return new Response($html);
    }

    /**
     * ajax
     * @Route("/admin/product/saveFields")
     * @param Request $request
     * @param AddField $addField
     * @return Response
     */
    public function saveAdditionalFields(Request $request, AddField $addField)
    {
        $fields = $request->get('fields');
        $fields = json_decode($fields, 1);
        if (count($fields) == 0) return new Response('ok');
        $productId = $fields[0]['productId'];
        $rootCategory = $fields[0]['rootCategory'];
        $addField->removeFieldsByProductId($productId);
        foreach ($fields as $field) {
            if($field['fieldDataType'] == 'int') $field['fieldValue'] = intval($field['fieldValue']);
            if($field['fieldDataType'] == 'float') {
                $field['fieldValue'] = str_replace(' ', '', $field['fieldValue']);
                $field['fieldValue'] = str_replace(',', '.', $field['fieldValue']);
                $field['fieldValue'] = floatval($field['fieldValue']);
            }
            $addField->saveAdditionalField($productId, $rootCategory, $field['fieldName'], $field['fieldValue'],
                $field['fieldDataType'], $field['fieldItem']);
        }
        return new Response('ok');
    }

    /**
     * ajax
     * @Route("/admin/product/getRootCategory")
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function getRootCategory(Request $request, Category $category)
    {
        $href = $request->get('categoryHref');
        $categoryName = explode('/', $href)[2];
        $rootCategoryName = $category->getRootCategoryByName($categoryName);
        return new Response($rootCategoryName);
    }

    /**
     * ajax
     * @Route("/admin/product/updateCategories")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function updateProductCategories(Request $request, Product $product)
    {
        $productId = $request->get('productId');
        $category = $request->get('category');
        $rootCategory = $request->get('rootCategory');
        $product->updateProductCategories($productId, $category, $rootCategory);
        return new Response('ok');
    }


}
