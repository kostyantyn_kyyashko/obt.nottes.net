<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

trait AdminControllerTrait
{
    use ControllerTrait;

    protected function renderAdminPage($title = '', $content = '', $jsArray = [], $styles = [])
    {
            $html = $this->renderView('admin/common/header.html.twig', [
                'styles' => $styles
            ]);
            $html .= $this->renderView('admin/common/main.html.twig', [
                'title' =>$title,
                'content' => $content
            ]);
            $html .= $this->renderView('admin/common/footer.html.twig', [
                'scripts' => $jsArray
            ]);
        return $html;
    }

}
