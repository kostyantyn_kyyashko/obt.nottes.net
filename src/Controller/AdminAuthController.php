<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminAuthController extends  Controller
{
    /**
     * @Route("/admin")
     * @param Request $request
     * @return Response
     */
    public function admin(Request $request, AdminAuth $adminAuth)
    {
/*        if (self::checkCookie($request)) {
            //header('Location: /dashBoard');
            die();
        }*/
        $login = $request->get('login');
        $password = $request->get('password');
        if (!isset($login)) {
            $html = $this->renderView('admin/common/header.html.twig');
            $html .= $this->renderView('admin/common/login.html.twig');
            $html .= $this->renderView('admin/common/footer.html.twig');
            return new Response($html);
        }
        $admin = $adminAuth->getAdmin($login, $password);
        if (!$admin) {
            $html = $this->renderView('admin/common/header.html.twig');
            $html .= $this->renderView('admin/common/addAdmin.html.twig', ['message' => 'Auth Error']);
            $html .= $this->renderView('admin/common/login.html.twig');
            $html .= $this->renderView('admin/common/footer.html.twig');
            return new Response($html);
        }
        $uid = $admin[0]['_id']->__toString();
        setcookie('uid', $uid, time()+365*24*3600, '/');
        return $this->redirect('/dashBoard');
    }

    /**
     * @Route("/addAdmin")
     * @param Request $request
     * @return Response
     */
    public function addAdmin(Request $request, AdminAuth $adminAuth)
    {
        $html = $this->renderView('admin/common/header.html.twig');
        $html .= $this->renderView('admin/common/addAdmin.html.twig', ['message' => 'Add Admin User']);
        $login = trim($request->get('login'));
        $password = trim($request->get('password'));
        if ($login && $password) {
            try {
                $adminAuth->insertRow([
                    'login' => $login,
                    'hash' => md5($password),
                ]);
                $html .= $this->renderView('admin/common/addAdmin.html.twig', ['message' => 'SUCCESS creation admin with name: ' . $login]);
            }
            catch (\Exception $e) {
                $html .= $this->renderView('admin/common/addAdmin.html.twig', ['message' => 'ERROR creation admin with name: ' . $login]);
            }
        }
        $html .= $this->renderView('admin/common/login.html.twig');
        $html .= $this->renderView('admin/common/footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/dashBoard")
     * @param Request $request
     * @return Response
     */
    public function dashBoard(Request $request)
    {
        self::checkCookie($request);
        $html = $this->renderView('admin/common/header.html.twig');
        $html .= $this->renderView('admin/common/main.html.twig', ['content' => '']);
        $html .= $this->renderView('admin/common/footer.html.twig');
        return new Response($html);
    }

    public static function checkCookie(Request $request)
    {
        $uid = $request->cookies->get('uid');
        $dm = MongoManager::getInstance()->createManager();
        try {
            $user = $dm->createQueryBuilder('\App\Document\Items\AdminAuth')
                ->hydrate(false)
                ->field('_id')->equals($uid)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }

        if (!count($user)) {
            header('Location: /admin');
            die();
        }
        return true;
    }

    /**
     * @Route("/logOut")
     * @param Request $request
     * @return Response
     */
    public function logOut(Request $request)
    {
        setcookie('uid', null);
        return $this->redirect('/admin');
    }

    /**
     * @Route("/cacheClear")
     * @param Request $request
     * @return Response
     */
    public function cacheClear(Request $request)
    {
        //ob_start();
        $z = shell_exec('cd /var/www/obt.nottes.net; php bin/console cache:clear --no-warmup');
        return new Response($z);
    }

}
