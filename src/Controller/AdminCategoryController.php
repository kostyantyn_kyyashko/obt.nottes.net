<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\Items\Category;
use App\Document\MongoManager;
use App\Document\MongoManager3;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminCategoryController extends  Controller
{
    use AllControllerTrait;
    private $tree;
    /**
     * @Route("/admin/category/add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        AdminAuthController::checkCookie($request);
        $content = $this->renderView('admin/category/add.html.twig');
        $scripts = [
            '/js/admin/category/init.js'
        ];
        $html = $this->renderAdminPage('Добавление категории', $content, $scripts);

        return new Response($html);
    }

    /**
     * Ajax save
     * @Route("/admin/category/save")
     * @param Request $request
     * @return Response
     */
    public function save(Request $request, Category $category)
    {
        $data = [
            'categoryLevel' => $request->get('categoryLevel'),
            'categoryParent' => $request->get('categoryParent'),
            'categoryName' => $request->get('categoryName'),
        ];
        $category->insertRow($data);
        return new Response('ok');
    }

    /**
     * Ajax get parent level of categories
     * @Route("/admin/category/getByLevel")
     * @param Request $request
     * @return Response
     */
    public function getParentCategoriesByLevel(Request $request, Category $category)
    {
        $level = intval($request->get('categoryLevel')) - 1;
        $parents = $category->getParentCategoriesByLevel($level);
        $html = $this->renderView('admin/category/parentSelectContent.html.twig', ['parents' => $parents]);
        return new Response($html);
    }

    /**
     * Ajax get parent level of categories
     * @Route("/admin/category/getTree")
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function getCatyegoriesTree(Request $request, Category $category)
    {
        AdminAuthController::checkCookie($request);
        $tree = $category->getTree(); //Utils::debugView($tree, 1);
        $treeContent = $this->showCat($tree);
        $treeContent = $this->renderView('admin/category/treeContent.html.twig', [
            'treeContent' => $treeContent,
        ]);
        $scripts = [
            '/js/admin/category/init.js'
        ];
        $html = $this->renderAdminPage('', $treeContent, $scripts);

        return new Response($html);
    }

    /**
     * Ajax get parent level of categories
     * @Route("/admin/category/getTreeGoods")
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function getCatyegoriesTreeGoods(Request $request, Category $category)
    {
        AdminAuthController::checkCookie($request);
        $tree = $category->getTree(); //Utils::debugView($tree, 1);
        $treeContent = $this->showCatGoods($tree);
        $treeContent = $this->renderView('admin/category/treeContent.html.twig', [
            'treeContent' => $treeContent,
        ]);
        $scripts = [
            '/js/admin/category/init.js'
        ];
        $html = $this->renderAdminPage('', $treeContent, $scripts);

        return new Response($html);
    }


    /**
     * @param $category
     * @return string
     */
    private function tplMenu($category){
        if(!isset($category['categoryId'])) {
//            Utils::debugView($category, 1);
            //$category = array_values($category['childs'])[0];
            return '';
        }

        $menu = '<li>
        <a href="/admin/category/edit/' . $category['categoryId'] . '">'.
            $category['categoryName'].'</a>';

        if(isset($category['childs'])){
            $menu .= '<ul>'. $this->showCat($category['childs']) .'</ul>';
        }
        $menu .= '</li>';

        return $menu;
    }

    /**
     * @param $tree
     * @return string
     */
    public function showCat($tree){
        $string = '';
        foreach($tree as $item){
            $string .= $this->tplMenu($item);
        }
        return $string;
    }

    /**
     * @param $category
     * @return string
     */
    private function tplMenuGoods($category){
        if(!isset($category['categoryId'])) {
//            Utils::debugView($category, 1);
            //$category = array_values($category['childs'])[0];
            return '';
        }

        $menu = '<li>
        <a href="/admin/goodsByCategory/' . $category['categoryId'] . '">'.
            $category['categoryName'].'</a>';

        if(isset($category['childs'])){
            $menu .= '<ul>'. $this->showCatGoods($category['childs']) .'</ul>';
        }
        $menu .= '</li>';

        return $menu;
    }

    /**
     * @param $tree
     * @return string
     */
    public function showCatGoods($tree){
        $string = '';
        foreach($tree as $item){
            $string .= $this->tplMenuGoods($item);
        }
        return $string;
    }

    /**
     * Ajax get parent level of categories
     * @Route("/admin/category/edit/{_id}")
     * @param Request $request
     * @return Response
     */
    public function categoryEdit(Request $request, $_id, Category $category)
    {
        $categoryData = $category->getCategoryByCategoryId($_id);
        $content = $this->renderView('admin/category/edit.html.twig', ['category' => $categoryData]);
        $scripts = [
            '/js/admin/category/init.js'
        ];
        $html = $this->renderAdminPage('Редактирование категории', $content, $scripts);

        return new Response($html);
    }

    /**
     * Ajax save category changes
     * @Route("/admin/category/saveChanges")
     * @param Request $request
     * @return Response
     */
    public function saveCategoryChanges(Request $request, Category $category)
    {
        $categoryName = $request->get('categoryName');
        $_id = $request->get('_id');
        $categoryWeight = $request->get('categoryWeight');
        $category->saveCategoryDataById($_id, $categoryName, $categoryWeight);
        return new Response('ok');
    }

    /**
     * Ajax save category changes
     * @Route("/admin/category/upload")
     * @param Request $request
     * @return Response
     */
    public function saveCategoryFile(Request $request, Category $category)
    {
        global $kernel;
        $status = [
            'result' => 'error',
        ];
        $rootDir = $kernel->getProjectDir();
        $uploadPath = $rootDir . '/' . $this->getParameter('app.category_upload_path');
        $file = $request->files->get('categoryFile');
        $_id = $request->get('_id');
        if(!is_null($file) && preg_match('/image/', $file->getMimeType())){
            $filename = uniqid().".".$file->getClientOriginalExtension();
            $file->move($uploadPath, $filename);
        }
        else {
            return new Response(json_encode($status));
        }
        $category->saveCategoryFile($_id, $filename);
        $status = [
            'result' => 'ok',
            'file' => $this->getParameter('app.category_upload_url') . $filename,
        ];
        return new Response(json_encode($status));
    }

    /**
     * Ajax delete category
     * @Route("/admin/category/delete")
     * @param Request $request
     * @return Response
     */
    public function deleteCategory(Request $request, Category $category)
    {
        $_id = $request->get('_id');
        $category->deleteCategoryById($_id);
        return new Response('ok');
    }

}
