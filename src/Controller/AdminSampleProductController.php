<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\Items\Category;
use App\Document\Items\SampleProduct;
use App\Document\MongoManager;
use App\Document\MongoManager3;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminSampleProductController extends  Controller
{
    /**
     * @Route("/admin/sampleProduct/add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request, Category $category)
    {
        AdminAuthController::checkCookie($request);
        $rootCategories = $category->getRootCategories();
        $content = $this->renderView('admin/sampleProduct/add.html.twig', ['categories' => $rootCategories]);
        $html = $this->renderView('admin/common/header.html.twig');
        $html .= $this->renderView('admin/common/main.html.twig', [
            'title' => 'Образец товара',
            'content' => $content,
        ]);
        $html .= $this->renderView('admin/common/footer.html.twig', ['scripts' => [
            '/js/admin/sampleProduct/init.js'
        ]]);
        return new Response($html);
    }

    /**
     * ajax - field's html
     * @Route("/admin/sampleGood/addField")
     * @param Request $request
     * @return Response
     */
    public function addField(Request $request)
    {
        $html = $this->renderView('admin/sampleProduct/addField.html.twig');
        $result = json_encode([
            'html' => $html,
            'result' => 'ok'
        ]);
        return new Response($result);
    }

    /**
     * @Route("/admin/sampleProduct/save")
     * @param Request $request
     * @return Response
     */
    public function save(Request $request, MongoManager3 $dm)
    {
        $fields = $request->get('fields');
        $fields = json_decode($fields, 1);
        $categoryId = $fields[0]['categoryId'];
        $builder = $dm->createManager()->createQueryBuilder(SampleProduct::class);
        try {
            $builder
                ->remove()
                ->field('categoryId')->equals($categoryId)
                ->getQuery()
                ->execute();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }
        foreach ($fields as $index => $field) {
            try {
                $builder->insert();
                foreach ($field as $name => $value) {
                    $builder
                        ->field($name)->set(trim($value));
                }
                $builder
                    ->getQuery()
                    ->execute();
            }
            catch (\Exception $exception) {
                echo $exception->getMessage();
            }
        }
        return new Response('ok');
    }

    /**
     * @Route("/admin/sampleProduct/list")
     * @param Request $request
     * @param MongoManager3 $dm
     */
    public function sampleProductList(Request $request, SampleProduct $sampleProduct)
    {
        $out = $sampleProduct->getCategories();
        Utils::debugDump($out, 1);

    }

}
