<?php
namespace App\Controller;
use App\Core\XmlParser;
use App\Document\Items\AddField;
use App\Document\Items\Good;
use App\Document\Items\Order;
use App\Document\Items\OrderIdPhone;
use App\Document\Items\OrderProduct;
use App\Document\Items\Page;
use App\Document\Items\Category;
use App\Document\Items\Product;
use App\Document\Items\Slider;
use App\Document\Items\User;
use App\Document\Utils;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\Logger;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends  Controller
{
    use AllControllerTrait;
    /**
     * @Route("/")
     * @param Request $request
     * @param Category $category
     * @param Page $page
     * @return Response
     */
    public function main(Request $request, Page $page, Category $category, Slider $slider)
    {
        $html = $this->renderHeader($request, $category);
        $sliders = $slider->selectBy();
        $html .= $this->renderView('front/index/slider.html.twig', ['sliders' => $sliders]);
        $html .= $page->getPageBySlug('why_me');
        $categoriesLevel0 = $category->getRootCategories();
        $html .= $this->renderView('front/index/our_products.html.twig', ['categories_level_0' => $categoriesLevel0]);
        $html .= $this->renderView('front/index/liders.html.twig');
        $html .= $page->getPageBySlug('about_index');
        $html .= $this->renderView('front/index/brands.html.twig');
        $html .= $this->renderView('front/index/horizontal_banner.html.twig');
        $html .= $this->renderView('front/common/footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/about")
     * @param Request $request
     * @param Category $category
     * @param Page $page
     * @return Response
     */
    public function about(Request $request, Page $page, Category $category)
    {
        $html = $this->renderHeader($request, $category);
        $html .= $page->getPageBySlug('about');
        $html .= $this->renderView('front/common/footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/contacts")
     * @param Request $request
     * @param Category $category
     * @param Page $page
     * @return Response
     */
    public function contacts(Request $request, Page $page, Category $category)
    {
        $html = $this->renderHeader($request, $category);
        $html .= $page->getPageBySlug('contacts');
        $html .= $this->renderView('front/common/footer.html.twig', [
            'scripts' => ['/js/contact.js']
        ]);
        return new Response($html);
    }

    /**
     * @Route("/catalog")
     * @param Request $request
     * @param Category $category
     * @param Page $page
     * @return Response
     */
    public function catalog(Request $request, Category $category, Page $page)
    {
        $categoriesLevel0 = $category->getRootCategories();
        $content = $this->renderView('front/catalog/catalog_content.html.twig', [
            'categoriesLevel0' => $categoriesLevel0,
            'seo_text_catalog' => $page->getPageBySlug('seo_text_catalog'),

        ]);
        $html = $this->renderFrontPage($request, $category, $content);
        return new Response($html);
    }

    /**
     * @Route("/product/{goodId}")
     * @param Request $request
     * @param Category $category
     * @param Product $product
     * @param Page $page
     * @param AddField $addField
     * @param $productId
     * @@return Response
     */
    public function productPage(Request $request, Category $category,
                                Good $good, Page $page, AddField $addField, $goodId)
    {
        $viewedProduct = $good->getGoogBygoodId($goodId);
        //$fields = $addField->getFieldsByProductId($goodId);
        $fields = [];
/*        if($viewedProduct['image']) {
            $images = json_decode($viewedProduct['files'], 1);
        }
        else {
            $images = [false];
        }*/
        $images = isset($viewedProduct['image'])&&$viewedProduct['image'] ?$viewedProduct['image']:false;
        //Utils::debugView($viewedProduct, 1);
        $content = $this->renderView('front/product/page.html.twig', [
            'product' => $viewedProduct,
            'images' => $images,
            'product_footer' => $page->getPageBySlug('product_footer'),
            'fields' => $fields,
        ]);
        $html = $this->renderFrontPage($request, $category, $content);
        return new Response($html);
    }

    /**
     * @Route("/card");
     * @param Request $request
     * @param Product $product
     * @param Category $category
     * @param AddField $addField
     * @return Response
     */
    public function card(Request $request, Good $good, AddField $addField, Category $category)
    {
        $cardContent = $request->cookies->get('card');
        $cardContent = json_decode($cardContent, 1);
        $items = [];
        $totalPrice = 0;
        if (is_array($cardContent)) {
            foreach ($cardContent as $data) {
                $item = $good->getGoogBygoodId($data['product_id']);
                $item['count'] = $data['product_count'];
                $totalPrice += $item['count']*$item['price'];
                $item['fields'] = $addField->getFieldsByProductId($data['product_id']);
                $items[] = $this->renderView('front/card/card_item.html.twig', ['item' => $item]);
            }
        }
        else {
            $items[] = $this->renderView('front/card/empty.html.twig');
        }
        //$categoriesLevel0 = $category->getRootCategories();
        $html = $this->renderHeader($request, $category);
        $html .= $this->renderView('front/card/card.html.twig', ['items' => $items, 'totalPrice' => $totalPrice]);
        $html .= $this->renderView('front/common/footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/order")
     * @param Request $request
     * @param Product $product
     * @param Category $category
     * @param Order $order
     * @return Response
     */
    public function order(Request $request, Good $good, Order $order, Category $category, User $user)
    {
        $cardContent = $request->cookies->get('card'); //Utils::debugView($cardContent, 1);
        $cardContent = json_decode($cardContent, 1);
        $totalPrice = 0;
        if (is_array($cardContent)) {
            foreach ($cardContent as $data) {
                $item = $good->getGoogBygoodId($data['product_id']);
                $item['count'] = $data['product_count'];
                $totalPrice += $item['count']*$item['price'];
            }
        }
        else {
            return $this->redirect('/catalog');
        }
        $html = $this->renderHeader($request, $category);
        $userId = $request->cookies->get('userId');
        $userData = $user->getUserByUserId($userId);
        $html .= $this->renderView('front/order/order.html.twig', [
            'total_price' => $totalPrice,
            'user_data' => $userData,
        ]);
        $html .= $this->renderView('front/common/footer.html.twig');
        return new Response($html);

    }

    /**
     * ajax
     * @Route("/createOrder")
     * @param Request $request
     * @param Order $order
     * @param OrderProduct $orderProduct
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function createOrder(Request $request, Order $order, OrderProduct $orderProduct, User $user, OrderIdPhone $orderIdPhone)
    {
        $orderId = mt_rand(10000, 1000000);
        $orderContent = $request->get('order_content');
        $orderContent = json_decode($orderContent, 1);
        if (is_array($orderContent)) {
            foreach ($orderContent as $product) {
                $product['orderId'] = $orderId;
                $orderProduct->insertRow($product);
            }
        }
        $orderData = $request->get('data');
        $orderData = json_decode($orderData, 1);
        $orderData['phone'] = preg_replace('/[^\d]/', '', $orderData['phone']);
        $userData = $user->getUserByPhone($orderData['phone']);
        if ($userData['status'] == 'fail') {
            $user->insertUser($orderData);
        }
        $orderData['orderId'] = $orderId;
        $orderData['status'] = 'Новый';
        $orderData['time_stamp'] = time();
        $order->insertRow($orderData);
        $orderIdPhone->insertRow([
            'orderId' => $orderId,
            'phone' => $orderData['phone'],
        ]);
        $resp = [
            'status' => 'ok',
            'orderId' => $orderId
        ];
        return new Response(json_encode($resp));
    }

    /**
     * ajax
     * @Route("/order/get_user_by_phone")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function getUserByPhone(Request $request, User $user)
    {
        $phone = $request->get('phone');
        $userData = $user->getUserByPhone($phone);
        return new Response(json_encode($userData));
    }

    /**
     * @Route("/cabinet")
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function personalCabinet(Request $request, Category $category, User $user, Order $order, Good $good)
    {
        $userId = $request->cookies->get('userId');
        if (!$userId) return $this->redirect('/');
        $userData = $user->getUserByUserId($userId);
        $orders = $order->getOrderByPhone($userData['phone']);
        $out = [];
        foreach ($orders as $dataOrder) {
            $orderContent = json_decode($dataOrder['order_content'], 1);
            $products = [];
            foreach ($orderContent as $item) {
                $dataProduct = $good->getGoogBygoodId($item['product_id']);
                $dataProduct['product_count'] = $item['product_count'];
                $products[] = $dataProduct;
            }
            $dataOrder['products'] = $products;
            $out[] = $dataOrder;
        }
        //Utils::debugView($out, 1);
        $html = $this->renderHeader($request, $category);
        $html .= $this->renderView('front/cabinet/cabinet.html.twig', [
            'user_data' => $userData,
            'out' => $out
        ]);
        $html .= $this->renderView('front/common/footer.html.twig');
        return new Response($html);
    }

    /**
     * ajax
     * @Route("/sendSMS")
     * @param Request $request
     * @return Response
     */
    public function sendSMS(Request $request, User $user)
    {
        $phone = $request->get('phone');
        $smsValue = mt_rand(1000, 9999);
        $userData = $user->getUserByPhone($phone);
        if ($userData['status'] == 'fail') return new Response('fail');
        file_get_contents("https://smsc.ua/sys/send.php?login=konst20&psw=rbzirj25&phones=$phone&mes=$smsValue&sender=smsc.ru");

        $mail_header = "MIME-Version: 1.0\r\n";
        $mail_header.= "Content-type: text/plain; charset=utf-8\r\n"; //Кодировка письма
        $mail_header.= "From: obtekatel58.ru <info@obtekatel58.ru>\r\n"; //Наименование и почта
        $mail_header.= "Reply-to: Код проверки obtekatel58.ru <info@obtekatel58.ru>\r\n"; // Ответ
        $to = $userData['email']; //Почта получателя
        $subject = "Код проверки с obtekatel58.ru"; //Загаловок сообщения
        $mes = "Код проверки $smsValue";
        mail($to,$subject,$mes,$mail_header);

        return new Response(md5($smsValue));
    }

    /**
     * ajax
     * @Route("/checkSMS")
     * @param Request $request
     * @return Response
     */
    public function checkSMS(Request $request)
    {
        $value = $request->get('value');
        $hash = $request->get('hash');
        if (md5($value) == $hash) {
            $resp = 'ok';
        }
        else {
            $resp = 'fail';
        }
        return new Response($resp);
    }


    /**
     * ajax
     * @Route("/checkUser")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function checkUser(Request $request, User $user)
    {
        $phone = $request->cookies->get('phone');
        $userData = $user->getUserByPhone($phone);
        $userData['userId'] = $userData['_id']->__toString();
        unset($userData['_id']);
        return new Response(json_encode($userData));
    }


    /**
     * @Route("/orderSuccess")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function orderSuccess(Request $request, Category $category)
    {
        $order_id = $request->get('order_id');
        setcookie('card', null);
        if(!$order_id) return $this->redirect('/');
        $html = $this->renderHeader($request, $category);
        $html .= $this->renderView('front/order/success.html.twig', ['order_id' => $order_id]);
        $html .= $this->renderView('front/common/footer.html.twig');
        return new Response($html);
    }

    /**
     * @Route("/category/{categoryId}")
     * @param Request $request
     * @param Category $category
     * @param Product $product
     * @param Page $page
     * @param $categoryId
     * @return Response
     */
    public function productsInCategoryList(Request $request, Category $category, Good $good, Page $page, $categoryId)
    {
        $pageNumber = $request->get('page')?$request->get('page'):1;
        $order = $request->get('order');
        $goods = $good->getGoodByCategory($categoryId, $pageNumber, $order);
        $goods = $this->parseAnalog($goods);
        $childCategories = $category->getCategoriesByParentCategory($categoryId);
        $content = $this->renderView('front/catalog/child_categories.html.twig', [
            'child_categories' => $childCategories
        ]);
        $countProducts = count($good->getGoodByCategory($categoryId,1, null, 1000000));
        $paginator = $this->paginator($request, $countProducts, 3);
        $categoryData = $category->getCategoryByCategoryId($categoryId);
        $content .= $this->renderView('front/product/catalog2.html.twig', [
            'products' => $goods,
            'total_count' => $countProducts,
            'category_name' => $categoryData['categoryName'],
            'paginator' => $paginator,
        ]);
        $content .= $page->getPageBySlug('seo_text_catalog');
        $html = $this->renderFrontPage($request, $category, $content);
        return new Response($html);
    }

    /**
     * ajax
     * @Route("/updateUser")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function updateUser(Request $request, User $user)
    {
        $phone = $request->get('phone');
        $user_data = $request->get('user_data');
        $user->updateUser($phone, $user_data);
        return new Response('ok');
    }

    /**
     * @Route("/find")
     * @param Request $request
     * @param Good $good
     * @return Response
     * @throws \MongoException
     */
    public function find(Request $request, Good $good, Category $category)
    {
        $searchString = $request->get('search');
        $searchString = urldecode($searchString);
        $analog = $good->searchByAnalog($searchString);
        $analog = $this->parseAnalog($analog);
        $table_analog = $this->renderView('front/find/table.html.twig', ['products' => $analog]);

        $articul = $good->searchByArticul($searchString);
        $articul = $this->parseAnalog($articul);
        $table_articul = $this->renderView('front/find/table.html.twig', ['products' => $articul]);

        $name = $good->searchByName($searchString);
        $name = $this->parseAnalog($name);

        $partName = $good->searchByPartName($searchString);
        $partName = $this->parseAnalog($partName);

        foreach ($partName as $pn) {
            $zero = 0;
            foreach ($name as $n) {
                if ($pn['articul'] == $n['articul']) {
                    $zero = 1;
                }
            }
            if (!$zero) {
                $name[] = $pn;
            }
        }

        $table_name = $this->renderView('front/find/table.html.twig', ['products' => $name]);

/*        $partName = $good->searchByPartName($searchString);
        $partName = $this->parseAnalog($partName);
        $table_part_name = $this->renderView('front/find/table.html.twig', ['products' => $partName]);*/

        $content = $this->renderView('front/find/tabs.html.twig', [
            'search_string' => $searchString,
            'table_analog' => $table_analog,
            'table_articul' => $table_articul,
            'table_name' => $table_name,
            //'table_part_name' => $table_part_name
        ]);
        $html = $this->renderFrontPage($request, $category, $content);
        return new Response($html);
    }

    /**
     * @Route("/sendFeedback")
     * @param Request $request
     */
    public function sendFeedback(Request $request)
    {
        $name = $request->get('name');
        $phone = $request->get('phone');
        $mail_header = "MIME-Version: 1.0\r\n";
        $mail_header.= "Content-type: text/plain; charset=utf-8\r\n"; //Кодировка письма
        $mail_header.= "From: obtekatel58.ru <info@obtekatel58.ru>\r\n"; //Наименование и почта
        $mail_header.= "Reply-to: Код проверки obtekatel58.ru <info@obtekatel58.ru>\r\n"; // Ответ
        $to = 'konst21.spb@yandex.ru'; //Почта получателя
        $subject = "Форма обратной связи с obtekatel58.ru"; //Загаловок сообщения
        $mes = "Клиент: имя $name, телефон $phone";
        mail($to,$subject,$mes,$mail_header);
        return new Response('ok');
    }

    /**
     * @Route("/import_xml")
     * @param Request $request
     * @param XmlParser $xmlParser
     * @param Product $product
     * @return Response
     * @todo log import
     * @throws \Exception
     */
    public function importXml(Request $request, XmlParser $xmlParser, Product $product)
    {
        $xml = $xmlParser->rawXml();
        $offers = $xmlParser->getOffers($xml);
        $out = [];
        foreach ($offers as $offer) {
            $row = $xmlParser->parseOffer($offer);
            $row['description'] = '';
            $row['category'] = '';
            $row['files'] = '';
            $product->insertRow($row);
        }
        //Utils::debugView($out);
        return new Response('ok');
    }

    /**
     * @Route("/testq")
     */
    public function testq(Request $request, Good $good)
    {
        $file = '/var/www/obt.nottes.net/_offers/import.xml';
        $xml = simplexml_load_file($file);
        $res = json_decode(json_encode($xml), 1);
        $goods = (($res['Каталог']['Товары']['Товар']));
        $goods = $this->parseGoods($goods);
        foreach ($goods as $goodRow) {
            $good->insertRow($goodRow);
        }
        return new Response('ok');
    }

    public function parseGoods($goods)
    {
        $out = [];
        foreach ($goods as $good) {
            $out[] = [
                'goodId' => $good['Ид'],
                'barcode' => isset($good['Штрихкод'])?$good['Штрихкод']:null,
                'articul' => isset($good['Артикул'])?$good['Артикул']:null,
                'name' => $good['Наименование'],
                'analog' => str_replace('=', ' ',isset($good['Аналоги'])?$good['Аналоги']:null),
                'item' => $good['БазоваяЕдиница'],
                'group' => isset($good['Группы']['Ид'])?$good['Группы']['Ид']:null,
                'image' => isset($good['Картинка'])?$good['Картинка']:null,
                'price' => mt_rand(100, 10000),
            ];
        }
        return $out;
    }
}
