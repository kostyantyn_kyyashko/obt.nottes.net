<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\Items\Category;
use App\Document\Items\Page;
use App\Document\Items\User;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

trait AllControllerTrait
{
    use ControllerTrait;

    /**
     * @param Request $request
     * @return string
     */
    protected function getRoute(Request $request)
    {
        $route = $request->getPathInfo();
        $route = explode('/', $route)[1];
        $route = !$route?'index':$route;
        return $route;
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function renderHeader(Request $request, Category $category)
    {
        $user = new User();
        $uid = $request->cookies->get('userId');
        if ($uid) {
            $userData = $user->getUserByUserId($uid);
            $left_corner = $this->renderView('front/common/left_corner_user.html.twig', [
                'fio' => $userData['fio']
            ]);
        }
        else {
            $left_corner = $this->renderView('front/common/left_corner_enter.html.twig');
        }
        $categoriesLevel0 = $category->getRootCategories();
        $html = $this->renderView('front/common/header.html.twig', [
            'menu' => [
                $this->getRoute($request) => 'active'
            ],
            'categories_level_0' => $categoriesLevel0,
            'left_corner' => $left_corner
        ]);
        return $html;
    }

    /**
     * @param $category
     * @return string
     */
    protected function tplMenu($category){
        if(!isset($category['_id'])) {
            return '';
        }

        $menu = $this->renderView('front/catalog/li_a_item.html.twig', [
            'category' => $category,
        ]);

        if(isset($category['childs'])){
            $childs = $this->showCat($category['childs']);
            $childsLevel = $category['categoryLevel'] + 1;
            $menu .= $this->renderView('front/catalog/ul_dropdown.html.twig', [
                'childsLevel' => $childsLevel,
                'childs' => $childs,
            ]);
        }
        $menu .= '</li>';

        return $menu;
    }

    /**
     * @param $tree
     * @return string
     */
    protected function showCat($tree){
        $string = '';
        foreach($tree as $item){
            $string .= $this->tplMenu($item);
        }
        return $string;
    }

    /**
     * @param string $title
     * @param string $content
     * @param array $jsArray
     * @param array $styles
     * @return string
     */
    protected function renderAdminPage($title = '', $content = '', $jsArray = [], $styles = [])
    {
        $html = $this->renderView('admin/common/header.html.twig', [
            'styles' => $styles
        ]);
        $html .= $this->renderView('admin/common/main.html.twig', [
            'title' =>$title,
            'content' => $content
        ]);
        $html .= $this->renderView('admin/common/footer.html.twig', [
            'scripts' => $jsArray
        ]);
        return $html;
    }

    protected function renderFrontPage(Request $request, Category $category, $content)
    {
        $html = $this->renderHeader($request, $category);
        $tree = $category->getTree();
        $categoryTree = $this->renderView('front/catalog/wrap_ul.html.twig', [
            'tree' => $this->showCat($tree)
        ]);
        $html .= $this->renderView('front/common/content.html.twig', [
            'content' => $content,
            'categoryTree' => $categoryTree,
        ]);
        $html .= $this->renderView('front/index/horizontal_banner.html.twig');
        $html .= $this->renderView('front/common/footer.html.twig');
        return $html;
    }

    protected function paginator(Request $request, $totalItems, $buttonsCount = 3, $itemsPerPage = 10)
    {
        $mainURI = $request->getPathInfo();
        $GETrequest = $_GET;
        $uriPage = $request->get('page')?$request->get('page'):1;
        $lastPage = round($totalItems/$itemsPerPage) + 1;
        $buttonsHtml = '';
        $page = $uriPage;
        $edgButton = $page + $buttonsCount;
        while (($page < $edgButton && $page < $lastPage)) {
            $GETrequest['page'] = $page;
            $get = http_build_query($GETrequest);
            $buttonsHtml .= $this->renderView('front/paginator/button.html.twig', [
                'uri' => $mainURI,
                'get' => $get,
                'page' => $page,
            ]);
            $page++;
        }
        $GETrequest['page'] = 1;
        $get = http_build_query($GETrequest);
        $prevHtml = $this->renderView('front/paginator/button_prev.html.twig', [
            'uri' => $mainURI,
            'get' => $get,
        ]);
        $GETrequest['page'] = $lastPage;
        $get = http_build_query($GETrequest);
        $nextHtml = $this->renderView('front/paginator/button_next.html.twig', [
            'uri' => $mainURI,
            'get' => $get,
        ]);
        $paginatorHtml = $this->renderView('front/paginator/paginator.html.twig', [
            'button_prev' => $prevHtml,
            'buttons' => $buttonsHtml,
            'button_next' => $nextHtml,
        ]);
        return $paginatorHtml;
    }

    protected function parseAnalog($goods)
    {
        $out2 = [];
        if (is_array($goods)) {
            foreach ($goods as $good) {
                $analogs = $good['analog'];
                if ($analogs) {
                    $analogs = str_replace('=', ' ', $analogs);
                    $analogs = str_replace('//', ' ', $analogs);
                    $analogs = preg_replace('/\s{1,10}/', ' ', $analogs);
                    $analogs = explode(' ', $analogs);
                    $out = [];
                    if (is_array($analogs)) {
                        foreach ($analogs as $analog) {
                            $out[] = '<a href="/find?search=' . $analog . '">' . $analog . '</a>';
                        }
                    }
                    $analogs =  trim(implode(' ', $out));
                    $good['analog'] = $analogs;
                }
                $out2[] = $good;
            }
        }
        return $out2;
    }


}
