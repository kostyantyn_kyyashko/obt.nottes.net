<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\Items\Category;
use App\Document\Items\Good;
use App\Document\Items\Order;
use App\Document\Items\Product;
use App\Document\MongoManager;
use App\Document\MongoManager3;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminOrderController extends  Controller
{
    use AllControllerTrait;

    /**
     * @Route("/admin/order/list")
     * @param Request $request
     * @param Order $order
     * @return Response
     */
    public function orderList(Request $request, Order $order)
    {
        AdminAuthController::checkCookie($request);
        $orders = $order->getOrdersByStatus();
        $content = $this->renderView('front/order/list.html.twig', ['orders' => $orders]);

        $html = $this->renderAdminPage('Заказы', $content);
        return new Response($html);
    }

    /**
     * @Route("/admin/single_order/{orderId}")
     * @param Request $request
     * @param Order $order
     * @return Response
     */
    public function singleOrder(Request $request, Order $order, Good $good, $orderId)
    {
        AdminAuthController::checkCookie($request);
        $orderData = $order->getOrderByOrderId($orderId);
        $order_content = json_decode($orderData['order_content'], 1);
        foreach ($order_content as $item) {
            $productContent = $good->getGoogBygoodId($item['product_id']);
            $item['productId'] = $productContent['goodId'];
            $item['articul'] = $productContent['articul'];
            $item['name'] = $productContent['name'];
            $item['price'] = $productContent['price'];
            $item['item_price'] = $productContent['price']*$item['product_count'];
            $orderData['order_items'][] = $item;
        }
        //Utils::debugView($orderData, 1);
        $content = $this->renderView('front/order/single_order.html.twig', ['order' => $orderData]);
        $html = $this->renderAdminPage('Заказ <span id="order_id">' .$orderId . '</span>', $content, ['/js/order/init.js']);
        return new Response($html);
    }


    /**
     * @Route("/admin/order/changeStatus")
     * @param Request $request
     * @param Order $order
     * @return Response
     */
    public function changeStatus(Request $request, Order $order)
    {
        $status = $request->get('status');
        $orderId = $request->get('order_id');
        $order->changeOrderStatus($status, intval($orderId));
        return new Response('ok');
    }
}
