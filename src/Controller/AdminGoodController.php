<?php
namespace App\Controller;
use App\Document\Items\AddField;
use App\Document\Items\Category;
use App\Document\Items\Good;
use App\Document\Items\Page;
use App\Document\Items\Product;
use App\Document\MongoManager;
use App\Document\MongoManager3;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminGoodController extends  Controller
{
    use AllControllerTrait;

    /**
     * /admin/goodsByCategory/{categoryId}
     * @param Request $request
     * @param Category $category
     * @param Good $good
     */
    public function goodsByCategory(Request $request, Category $category, Good $good, $categoryId)
    {
        $goods = $good->getGoodByCategory($categoryId, 1);
    }
}
