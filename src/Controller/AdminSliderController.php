<?php
namespace App\Controller;
use App\Document\Items\AdminAuth;
use App\Document\Items\Slider;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminSliderController extends  Controller
{
    use AllControllerTrait;

    /**
     * @Route("/admin/slider/add")
     * @param Request $request
     * @return Response
     */
    public function slider(Request $request)
    {
        AdminAuthController::checkCookie($request);
        $content = $this->renderView('admin/slider/form.html.twig');
        $js = [
            '/js/admin/slider/bs-custom-file-input.js',
            '/js/admin/slider/init.js',
        ];
        $html = $this->renderAdminPage('Слайдер', $content, $js);
        return new Response($html);
    }

    /**
     * @Route("/admin/slider/list")
     * @param Request $request
     * @param Slider $slider
     * @return Response
     */
    public function sliderList(Request $request, Slider $slider)
    {
        AdminAuthController::checkCookie($request);
        $sliders = $slider->selectBy();
        $content = $this->renderView('admin/slider/list.html.twig', [
            'sliders' => $sliders
        ]);
        $js = [
            '/js/admin/slider/bs-custom-file-input.js',
            '/js/admin/slider/init.js',
        ];
        $html = $this->renderAdminPage('Слайдер', $content, $js);
        return new Response($html);
    }

    /**
     * upload data by Ajax
     * @Route("/admin/slider/upload")
     * @param Request $request
     * @return Response
     */
    public function uploadSliderFileAndData(Request $request)
    {
        global $kernel;
        $rootDir = $kernel->getProjectDir();
        $uploadPath = $rootDir . '/' . $this->getParameter('app.slider_upload_path');
        $file = $request->files->get('sliderFile');
        // If a file was uploaded
        if(!is_null($file)){
            $filename = uniqid().".".$file->getClientOriginalExtension();
            $file->move($uploadPath, $filename); // move the file to a path
        }

        $slider = new Slider();
        $slider->insertRow([
            'header1' => $request->get('header1'),
            'header2' => $request->get('header2'),
            'link' => $request->get('link'),
            'fileName' => $filename,
        ]);
        $resp = [
            'status' => 'ok',
            'file' => $this->getParameter('app.slider_upload_url').$filename,
        ];
        return new Response(json_encode($resp));
    }

    /**
     * @Route("/admin/slider/remove/{id}")
     * @param Request $request
     * @return Response
     */
    public function removeSlider(Request $request, $id)
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Slider');
        try {
            $sliders = $builder
                ->remove()
                ->field('_id')->equals($id)
                ->getQuery()
                ->execute();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $this->redirect('/admin/slider/list');
    }
}
