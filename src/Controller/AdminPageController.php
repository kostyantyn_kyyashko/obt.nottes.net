<?php
namespace App\Controller;
use App\Document\Items\Page;
use App\Document\MongoManager;
use App\Document\MongoManager3;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminPageController extends  Controller
{
    use AllControllerTrait;
    /**
     * @Route("/admin/page/add")
     * @param Request $request
     * @return Response
     */
    public function pageAdd(Request $request)
    {
        AdminAuthController::checkCookie($request);
        $url = $_SERVER['SERVER_NAME'];
        $content = $this->renderView('admin/page/urlAdd.html.twig', ['url' => $url]);
        $content .= $this->renderView('admin/page/addEdit.html.twig', ['pageContent' => '']);
        $scripts = [
            '/js/admin/page/init.js',
            '/js/admin/page/summernote2.js',
            '/js/admin/page/summernote-ru-RU.js',
        ];
        $styles = [
            '/css/style.css',
            '/css/admin/page_edit.css'
        ];
        $html = $this->renderAdminPage('Добавление страницы', $content, $scripts, $styles);
        return new Response($html);
    }

    /**
     * Save by Ajax
     * @Route("/admin/page/save")
     * @param Request $request
     * @param Page $page
     * @return Response
     * @throws \Exception
     */
    public function pageSave(Request $request, Page $page)
    {
        $slug = $request->get('slug');
        $pageContent = $request->get('pageContent');
        $pageDb = $page->selectBy(['slug' => $slug]);
        if (!count($pageDb)) {
            $page->insertRow([
                'slug' => $slug,
                'pageContent' => $pageContent
            ]);
        }
        else {
            $page->updateOne(['slug' => $slug], ['pageContent' => $pageContent]);
        }
        return new Response('ok');
    }

    /**
     * @Route("/admin/page/edit/{slug}")
     * @param Request $request
     * @return Response
     */
    public function pageEdit(Request $request, $slug, Page $page)
    {
        AdminAuthController::checkCookie($request);
        if (!$slug) return;
        $pageDb = $page->selectBy(['slug' => $slug]);

        if (!count($pageDb)) return;
        $pageDb = array_values($pageDb)[0];
        $url = $_SERVER['SERVER_NAME'];
        $content = $this->renderView('admin/page/urlView.html.twig', ['url' => $url, 'slug' => $pageDb['slug']]);
        $content .= $this->renderView('admin/page/addEdit.html.twig', ['pageContent' => $pageDb['pageContent']]);
        $scripts = [
            '/js/admin/page/init.js',
        ];
        $styles = [
            '/css/style.css',
            '/css/admin/page_edit.css'
        ];
        $html = $this->renderAdminPage('Редактирование страницы', $content, $scripts, $styles);
        return new Response($html);
    }

    /**
     * @Route("/admin/page/list")
     * @param Request $request
     * @param Page $page
     * @return Response
     */
    public function pageList(Request $request, Page $page)
    {
        AdminAuthController::checkCookie($request);
        $pageDb = $page->selectBy([], ['slug']);
        $slugs = [];
        foreach ($pageDb as $p) {
            $slugs[] = $p['slug'];
        }
        $content = $this->renderView('admin/page/list.html.twig', ['slugs' => $slugs]);
        $scripts = [
            '/js/admin/page/init.js',
        ];
        $html = $this->renderAdminPage('Добавление страницы', $content, $scripts);
        return new Response($html);
    }

    /**
     * @Route("/admin/page/remove/{slug}")
     * @param Request $request
     * @return Response
     */
    public function pageDelete(Request $request, $slug, Page $page)
    {
        AdminAuthController::checkCookie($request);
        $page->remove(['slug' => $slug]);
        return $this->redirect('/admin/page/list');
    }
}
