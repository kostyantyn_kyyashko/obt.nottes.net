<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Order
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"orderId"="asc"}),
 *     @MongoDB\Index(keys={"phone"="asc"}),
 *     @MongoDB\Index(keys={"email"="asc"})
 *     })
 *
 */
class Order extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $orderId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fio;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $userPhone;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $phone;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $city;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $street;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $house;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $flat;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $delivery_type;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $pay_type;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $total_price;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $delivery_cost;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $whole_price;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $status;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $order_content;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $time_stamp;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId(int $orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     */
    public function setFio(string $fio): void
    {
        $this->fio = $fio;
    }

    /**
     * @return string
     */
    public function getUserPhone(): string
    {
        return $this->userPhone;
    }

    /**
     * @param string $userPhone
     */
    public function setUserPhone(string $userPhone): void
    {
        $this->userPhone = $userPhone;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouse(): string
    {
        return $this->house;
    }

    /**
     * @param string $house
     */
    public function setHouse(string $house): void
    {
        $this->house = $house;
    }

    /**
     * @return string
     */
    public function getFlat(): string
    {
        return $this->flat;
    }

    /**
     * @param string $flat
     */
    public function setFlat(string $flat): void
    {
        $this->flat = $flat;
    }

    /**
     * @return string
     */
    public function getDeliveryType(): string
    {
        return $this->delivery_type;
    }

    /**
     * @param string $delivery_type
     */
    public function setDeliveryType(string $delivery_type): void
    {
        $this->delivery_type = $delivery_type;
    }

    /**
     * @return string
     */
    public function getPayType(): string
    {
        return $this->pay_type;
    }

    /**
     * @param string $pay_type
     */
    public function setPayType(string $pay_type): void
    {
        $this->pay_type = $pay_type;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->total_price;
    }

    /**
     * @param int $total_price
     */
    public function setTotalPrice(int $total_price): void
    {
        $this->total_price = $total_price;
    }

    /**
     * @return int
     */
    public function getDeliveryCost(): int
    {
        return $this->delivery_cost;
    }

    /**
     * @param int $delivery_cost
     */
    public function setDeliveryCost(int $delivery_cost): void
    {
        $this->delivery_cost = $delivery_cost;
    }

    /**
     * @return int
     */
    public function getWholePrice(): int
    {
        return $this->whole_price;
    }

    /**
     * @param int $whole_price
     */
    public function setWholePrice(int $whole_price): void
    {
        $this->whole_price = $whole_price;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getOrderContent(): string
    {
        return $this->order_content;
    }

    /**
     * @param string $order_content
     */
    public function setOrderContent(string $order_content): void
    {
        $this->order_content = $order_content;
    }

    /**
     * @return int
     */
    public function getTimeStamp(): int
    {
        return $this->time_stamp;
    }

    /**
     * @param int $time_stamp
     */
    public function setTimeStamp(int $time_stamp): void
    {
        $this->time_stamp = $time_stamp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function getOrdersByStatus($status='')
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $orders = [];
        $builder
            ->hydrate(false);
        if ($status) {
            $builder
                ->field('status')->equals($status);
        }
        try {
            $orders = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
        return array_values($orders);
    }

    /**
     * @param $orderId
     * @return |null
     */
    public function getOrderByOrderId($orderId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $order = null;
        try {
            $order = $builder
                ->hydrate(false)
                ->field('orderId')->equals(intval($orderId))
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
        if ($order) {
            return array_values($order)[0];
        }
        return $order;
    }

    /**
     * @param $orderId
     * @return |null
     */
    public function getOrderByPhone($phone)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $order = null;
        try {
            $order = $builder
                ->hydrate(false)
                ->field('phone')->equals($phone)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
        if ($order) {
            return ($order);
        }
        return $order;
    }

    /**
     * @param $status
     * @param $orderId
     */
    public function changeOrderStatus($status, $orderId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $builder->updateOne()
                ->field('status')->set($status)
                ->field('orderId')->equals($orderId)
                ->getQuery()
                ->execute();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
    }

}
