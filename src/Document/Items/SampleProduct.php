<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Slider
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"categoryId"="asc"})
 *     })
 *
 */
class SampleProduct extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $categoryId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fieldName;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $dataType;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $inFilter;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     */
    public function setFieldName(string $fieldName): void
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return string
     */
    public function getDataType(): string
    {
        return $this->dataType;
    }

    /**
     * @param string $dataType
     */
    public function setDataType(string $dataType): void
    {
        $this->dataType = $dataType;
    }

    /**
     * @return string
     */
    public function getInFilter(): string
    {
        return $this->inFilter;
    }

    /**
     * @param string $inFilter
     */
    public function setInFilter(string $inFilter): void
    {
        $this->inFilter = $inFilter;
    }

    public function getCategories()
    {
        $builder = MongoManager::getInstance()->createManager()
            ->createAggregationBuilder(SampleProduct::class);
        $res = $builder
            ->group()
            ->field('_id')
            ->expression(
                $builder->expr()
                    ->field('categoryId')
                    ->expression('$categoryId')
                    ->field('categoryName')
                    ->expression('$categoryName')
            )
            ->field('cc')
            ->sum(1)
            ->execute()
            ->toArray();
        $out = [];
        foreach ($res as $r) {
            $out[] = $r['_id'];
        }
        return $out;
    }


}
