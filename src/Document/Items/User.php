<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\Utils;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Category
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"phone"="asc"}, unique=true)
 *     })
 *
 */
class User extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $phone;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fio;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $city;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $street;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $house;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $flat;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     */
    public function setFio(string $fio): void
    {
        $this->fio = $fio;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouse(): string
    {
        return $this->house;
    }

    /**
     * @param string $house
     */
    public function setHouse(string $house): void
    {
        $this->house = $house;
    }

    /**
     * @return string
     */
    public function getFlat(): string
    {
        return $this->flat;
    }

    /**
     * @param string $flat
     */
    public function setFlat(string $flat): void
    {
        $this->flat = $flat;
    }

    /**
     * @param $phone
     * @return array
     */
    public function getUserByPhone($phone)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $user = $builder
                ->hydrate(false)
                ->field('phone')->equals($phone)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
        if ($user) {
            $user = array_values($user);
            if (isset($user[0])) {
                $data = $user[0];
                $data['status'] = 'ok';
                return $data;
            }
        }
        return ['status' => 'fail'];

    }

    /**
     * @param $userId
     * @return array
     */
    public function getUserByUserId($userId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $user = $builder
                ->hydrate(false)
                ->field('_id')->equals($userId)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
        if (isset($user) && $user) {
            $user = array_values($user);
            if (isset($user[0])) {
                $data = $user[0];
                return $data;
            }
        }
        return false;
    }

    /**
     * @param $data
     */
    public function insertUser($data)
    {
        unset($data['delivery_type']);
        unset($data['pay_type']);
        unset($data['total_price']);
        unset($data['delivery_cost']);
        unset($data['whole_price']);
        unset($data['order_content']);
        try {
            $this->insertRow($data);
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
    }

    public function updateUser($phone, $user_data)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder
            ->updateOne()
            ->field('phone')->equals($phone)
            ->field('fio')->set($user_data['fio'])
            ->field('email')->set($user_data['email'])
            ->field('city')->set($user_data['city'])
            ->field('street')->set($user_data['street'])
            ->field('house')->set($user_data['house'])
            ->field('flat')->set($user_data['flat']);
        try {
            $builder
                ->getQuery()
                ->execute();
        }
        catch (\Exception $exception){
            Utils::log($exception->getMessage());
        }
    }

}
