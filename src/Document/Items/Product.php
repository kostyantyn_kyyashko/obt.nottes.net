<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\Utils;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Category
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"category"="asc"}),
 *     @MongoDB\Index(keys={"rootCategory"="asc"}),
 *     @MongoDB\Index(keys={"name"="text"}),
 *     @MongoDB\Index(keys={"productId"="asc"}, unique=true)
 *     })
 *
 */
class Product extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $productId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $name;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $price;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $description;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $category;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $rootCategory;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $item;

    /**
     * @MongoDB\Field(type="hash")
     * @var array
     */
    protected $files;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getRootCategory(): string
    {
        return $this->rootCategory;
    }

    /**
     * @param string $rootCategory
     */
    public function setRootCategory(string $rootCategory): void
    {
        $this->rootCategory = $rootCategory;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @param string $item
     */
    public function setItem(string $item): void
    {
        $this->item = $item;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * @param array $files
     */
    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    /**
     * @param $productId
     * @return |null
     */
    public function getProductByProductId($productId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $product = $builder
                ->hydrate(false)
                ->field('productId')->equals($productId)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            return null;
        }
        $product = array_values($product);
        if (isset($product[0])) return $product[0];
        return false;
    }

    /**
     * @param int $page
     * @param null $sort
     * @param string $order
     * @return |null
     */
    public function getProductsPage($page = 1, $sort = null, $order = 'asc')
    {
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->skip(($page-1)*50)
            ->limit(50);
        if ($sort) {
            $builder
                ->sort($sort, $order);
        }
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            return null;
        }
        return $products;
    }

    /**
     * @param $searchString
     * @return |null
     * @throws \MongoException
     */
    public function searchProductsByRegEx($searchString)
    {
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->field('name')->equals(new \MongoRegex("/$searchString/"));
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            return null;
        }
        return $products;
    }

    /**
     * @param $searchString
     * @return |null
     */
    public function searchProductsByText($searchString)
    {
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->text($searchString);
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            return null;
        }
        return $products;
    }

    /**
     * @return int
     */
    public function getProductsCount()
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $count = $builder
                ->getQuery()
                ->execute()
                ->count();
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
            return 0;
        }
        return $count;
    }

    /**
     * @param $productId
     * @param $category
     * @param $rootCategory
     */
    public function updateProductCategories($productId, $category, $rootCategory)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $builder
                ->updateOne()
                ->field('productId')->equals($productId)
                ->field('category')->set($category)
                ->field('rootCategory')->set($rootCategory)
                ->getQuery()
                ->execute();
        }
        catch (\Exception $exception) {
            Utils::log($exception->getMessage());
        }
    }

    /**
     * @param $category
     * @return |null
     */
    public function getProductByCategory($category, $page, $sort = null, $resultsPerPage = 20)
    {
        $skip = ($page-1)*$resultsPerPage;
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->field('category')->equals($category)
            ->limit($resultsPerPage)
            ->skip($skip);
        if ($sort) {
            $builder
                ->sort('price', $sort);
        }
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return [];
        }
        return $products;

    }

}
