<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Page
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"slug"="asc"}, unique=true)
 *     })
 *
 */
class Page extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $slug;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $pageContent;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getPageContent(): string
    {
        return $this->pageContent;
    }

    /**
     * @param string $pageContent
     */
    public function setPageContent(string $pageContent): void
    {
        $this->pageContent = $pageContent;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getPageBySlug($slug)
    {
        $builder = MongoManager::getInstance()->createManager()->createQueryBuilder('\App\Document\Items\Page');
        try {
            $page = $builder
                ->hydrate(false)
                ->field('slug')->equals($slug)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
        }
        $page = array_values($page);
        if (isset($page[0])) {
            return $page[0]['pageContent'];
        }
        return false;
    }

}
