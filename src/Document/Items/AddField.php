<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Category
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"productId"="asc"}),
 *     @MongoDB\Index(keys={"fieldName"="asc"})
 *     })
 *
 */
class AddField extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $productId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fieldName;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fieldValue;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fieldItem;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     */
    public function setFieldName(string $fieldName): void
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return string
     */
    public function getFieldValue(): string
    {
        return $this->fieldValue;
    }

    /**
     * @param string $fieldValue
     */
    public function setFieldValue(string $fieldValue): void
    {
        $this->fieldValue = $fieldValue;
    }

    /**
     * @return string
     */
    public function getFieldItem(): string
    {
        return $this->fieldItem;
    }

    /**
     * @param string $fieldItem
     */
    public function setFieldItem(string $fieldItem): void
    {
        $this->fieldItem = $fieldItem;
    }

    /**
     * @param $productId
     */
    public function removeFieldsByProductId($productId)
    {
        $builder = $this->manager->createQueryBuilder(self::class);
        $builder
            ->remove()
            ->field('productId')->equals($productId);
        try{
            $builder
                ->getQuery()
                ->execute();
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * @param $productId
     * @param $fieldName
     * @param $fieldValue
     * @param $fieldItem
     */
    public function saveAdditionalField($productId, $rootCategory, $fieldName, $fieldValue, $fieldDataType, $fieldItem)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder
            ->insert()
            ->field('productId')->set($productId)
            ->field('rootCategory')->set($rootCategory)
            ->field('fieldName')->set($fieldName)
            ->field('fieldValue')->set($fieldValue)
            ->field('fieldDataType')->set($fieldDataType)
            ->field('fieldItem')->set($fieldItem);
        try {
            $builder
                ->getQuery()
                ->execute();
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function getFieldsByProductId($productId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $fields = $builder
                ->hydrate(false)
                ->field('productId')->equals($productId)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        return $fields;
    }



}
