<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class AdminAuth
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"orderId"="asc"}),
 *     @MongoDB\Index(keys={"productId"="asc"})
 *     })
 *
 */
class OrderProduct extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $orderId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $product_id;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $product_count;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId(int $orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->product_id;
    }

    /**
     * @param string $product_id
     */
    public function setProductId(string $product_id): void
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getProductCount(): int
    {
        return $this->product_count;
    }

    /**
     * @param int $product_count
     */
    public function setProductCount(int $product_count): void
    {
        $this->product_count = $product_count;
    }

    public function getCountByOrderIdProductId($orderId, $productId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        $count = $builder
            ->hydrate(false)
            ->select('product_count')
            ->field('orderId')->equals($orderId)
            ->field('product_id')->equals($productId)
            ->getQuery()
            ->execute()
            ->toArray();

        return $count;
    }
}
