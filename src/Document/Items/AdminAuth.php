<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class AdminAuth
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"login"="asc"}, unique=true)
 *     })
 *
 */
class AdminAuth extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $login;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $hash;

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @param $login
     * @param $password
     * @return int
     */
    public function getAdmin($login, $password)
    {
        $dm = MongoManager::getInstance()->createManager();
        try {
            $admin = $dm->createQueryBuilder(self::class)
                ->hydrate(false)
                ->field('login')->equals($login)
                ->field('hash')->equals(md5($password))
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }
        return array_values($admin);
    }

}
