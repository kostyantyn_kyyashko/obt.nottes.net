<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\Utils;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Good
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"goodId"="asc"}, unique=true),
 *     @MongoDB\Index(keys={"articul"="asc"}),
 *     @MongoDB\Index(keys={"name"="text"}),
 *     @MongoDB\Index(keys={"group"="asc"})
 *     })
 *
 */
class Good extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $goodId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $barcode;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $articul;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $analog;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $item;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $group;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGoodId(): string
    {
        return $this->goodId;
    }

    /**
     * @param string $goodId
     */
    public function setGoodId(string $goodId): void
    {
        $this->goodId = $goodId;
    }

    /**
     * @return string
     */
    public function getBarcode(): string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode(string $barcode): void
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getArticul(): string
    {
        return $this->articul;
    }

    /**
     * @param string $articul
     */
    public function setArticul(string $articul): void
    {
        $this->articul = $articul;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAnalog(): string
    {
        return $this->analog;
    }

    /**
     * @param string $analog
     */
    public function setAnalog(string $analog): void
    {
        $this->analog = $analog;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @param string $item
     */
    public function setItem(string $item): void
    {
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @param string $group
     */
    public function setGroup(string $group): void
    {
        $this->group = $group;
    }

    /**
     * @param $category
     * @return |null
     */
    public function getGoodByCategory($categoryId, $page, $sort = null, $resultsPerPage = 20)
    {
        $skip = ($page-1)*$resultsPerPage;
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->field('group')->equals($categoryId)
            ->limit($resultsPerPage)
            ->skip($skip);
/*        if ($sort) {
            $builder
                ->sort('price', $sort);
        }*/
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return [];
        }
        return $products;

    }

    /**
     * @param string $categoryId
     * @param int $page
     * @param null $sort
     * @param string $order
     * @return |null
     */
    public function getGoodsPage($categoryId, $page = 1, $sort = null, $order = 'asc')
    {
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->field('group')->equals($categoryId)
            ->skip(($page-1)*50)
            ->limit(50);
        if ($sort) {
            $builder
                ->sort($sort, $order);
        }
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return null;
        }
        return $products;
    }

    /**
     * @return int
     */
    public function getGoodsCount()
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $count = $builder
                ->getQuery()
                ->execute()
                ->count();
        }
        catch(\Exception $e)
        {
            Utils::log($e->getMessage());
            return 0;
        }
        return $count;
    }

    /**
     * @param $goodId
     * @return |null
     */
    public function getGoogBygoodId($goodId)
    {
        $builder = $this->manager->createQueryBuilder(static::class);
        try {
            $product = $builder
                ->hydrate(false)
                ->field('goodId')->equals($goodId)
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return null;
        }
        $product = array_values($product);
        if (isset($product[0])) return $product[0];
        return false;
    }

    /**
     * @param $searchString
     * @return |null
     * @throws \MongoException
     */
    public function searchGoodByRegEx($searchString)
    {
        $builder = $this->manager->createQueryBuilder(static::class)
            ->hydrate(false)
            ->field('name')->equals(new \MongoRegex("/$searchString/"));
        try {
            $products = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return null;
        }
        return $products;
    }

    /**
     * @param $searchString
     * @return array
     * @throws \MongoException
     */
    public function searchByArticul($searchString)
    {
        $result = [];
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder
            ->hydrate(false)
            ->field('articul')->equals(new \MongoRegex("/$searchString/"));
        try {
            $result = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return $result;
        }
        return array_values($result);

    }

    /**
     * @param $searchString
     * @return array
     * @throws \MongoException
     */
    public function searchByName($searchString)
    {
        $result = [];
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder
            ->hydrate(false)
            ->text($searchString);
        try {
            $result = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return $result;
        }
        return array_values($result);
    }

    /**
     * @param $searchString
     * @return array
     * @throws \MongoException
     */
    public function searchByAnalog($searchString)
    {
        $result = [];
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder
            ->hydrate(false)
            ->addOr($builder->expr()->field('analog')->equals(new \MongoRegex("/$searchString/")));
        try {
            $result = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return $result;
        }
        return array_values($result);
    }

    /**
     * @param $searchString
     * @return array
     * @throws \MongoException
     */
    public function searchByPartName($searchString)
    {
        $result = [];
        $builder = $this->manager->createQueryBuilder(static::class);
        $builder
            ->hydrate(false)
            ->addOr($builder->expr()->field('name')->equals(new \MongoRegex("/$searchString/")));
        try {
            $result = $builder
                ->getQuery()
                ->execute()
                ->toArray();
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage());
            return $result;
        }
        return array_values($result);
    }






}
