<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Slider
 * @package App\Document\Items
 * @MongoDB\Document(db="obt")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"fileName"="asc"}, unique=true)
 *     })
 *
 */
class Slider extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $header1;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $header2;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $link;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $fileName;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHeader1(): string
    {
        return $this->header1;
    }

    /**
     * @param string $header1
     */
    public function setHeader1(string $header1): void
    {
        $this->header1 = $header1;
    }

    /**
     * @return string
     */
    public function getHeader2(): string
    {
        return $this->header2;
    }

    /**
     * @param string $header2
     */
    public function setHeader2(string $header2): void
    {
        $this->header2 = $header2;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }



}
