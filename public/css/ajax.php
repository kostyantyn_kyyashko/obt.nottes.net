<?php

$config['smtp_username'] = 'imcompany@silaslovakids.ru';  //Смените на адрес своего почтового ящика.
$config['smtp_port'] = '465'; // Порт работы.
$config['smtp_host'] = 'ssl://smtp.yandex.ru';  //сервер для отправки почты
$config['smtp_password'] = 'silaslova';  //Измените пароль
$config['smtp_debug'] = true;  //Если Вы хотите видеть сообщения ошибок, укажите true вместо false
$config['smtp_charset'] = 'utf-8'; //кодировка сообщений. (windows-1251 или utf-8, итд)
$config['smtp_from'] = 'Сила Слова'; //Ваше имя - или имя Вашего сайта. Будет показывать при прочтении в поле "От кого"

function smtpmail($to = '', $mail_to, $subject, $message, $headers = '') {
    global $config;
    $SEND = "Date: " . date("D, d M Y H:i:s") . " UT\r\n";
    $SEND .= 'Subject: =?' . $config['smtp_charset'] . '?B?' . base64_encode($subject) . "=?=\r\n";
    if ($headers)
        $SEND .= $headers . "\r\n\r\n";
    else {
        $SEND .= "Reply-To: " . $config['smtp_username'] . "\r\n";
        $SEND .= "To: \"=?" . $config['smtp_charset'] . "?B?" . base64_encode($to) . "=?=\" <$mail_to>\r\n";
        $SEND .= "MIME-Version: 1.0\r\n";
        $SEND .= "Content-Type: text/html; charset=\"" . $config['smtp_charset'] . "\"\r\n";
        $SEND .= "Content-Transfer-Encoding: 8bit\r\n";
        $SEND .= "From: \"=?" . $config['smtp_charset'] . "?B?" . base64_encode($config['smtp_from']) . "=?=\" <" . $config['smtp_username'] . ">\r\n";
        $SEND .= "X-Priority: 3\r\n\r\n";
    }
    $SEND .= $message . "\r\n";
    if (!$socket = fsockopen($config['smtp_host'], $config['smtp_port'], $errno, $errstr, 30)) {
        if ($config['smtp_debug'])
            echo $errno . "<br>" . $errstr;
        return false;
    }

    if (!server_parse($socket, "220", __LINE__))
        return false;

    fputs($socket, "HELO " . $config['smtp_host'] . "\r\n");
    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Не могу отправить HELO!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "AUTH LOGIN\r\n");
    if (!server_parse($socket, "334", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Не могу найти ответ на запрос авторизаци.</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, base64_encode($config['smtp_username']) . "\r\n");
    if (!server_parse($socket, "334", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Логин авторизации не был принят сервером!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, base64_encode($config['smtp_password']) . "\r\n");
    if (!server_parse($socket, "235", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Пароль не был принят сервером как верный! Ошибка авторизации!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "MAIL FROM: <" . $config['smtp_username'] . ">\r\n");
    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Не могу отправить комманду MAIL FROM: </p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "RCPT TO: <" . $mail_to . ">\r\n");

    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Не могу отправить комманду RCPT TO: </p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "DATA\r\n");

    if (!server_parse($socket, "354", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Не могу отправить комманду DATA</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, $SEND . "\r\n.\r\n");

    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug'])
            echo '<p>Не смог отправить тело письма. Письмо не было отправленно!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "QUIT\r\n");
    fclose($socket);
    return TRUE;
}

function server_parse($socket, $response, $line = __LINE__) {
    global $config;
    while (@substr($server_response, 3, 1) != ' ') {
        if (!($server_response = fgets($socket, 256))) {
            if ($config['smtp_debug'])
                echo "<p>Проблемы с отправкой почты!</p>$response<br>$line<br>";
            return false;
        }
    }
    if (!(substr($server_response, 0, 3) == $response)) {
        if ($config['smtp_debug'])
            echo "<p>Проблемы с отправкой почты!</p>$response<br>$line<br>";
        return false;
    }
    return true;
}

//$to = 'r2403101@gmail.com, franchise@silaslovakids.ru';
// $to = 'stasgrigorev39@gmail.com';


$post = $_POST;

if ($func == 'plan') {
    $subject = 'На сайте silagolosakids.ru заполнена форма "Получить бизнес-план"';
} elseif ($func == 'callback') {
    $subject = 'На сайте silagolosakids.ru заполнена форма "Задать вопрос"';
} else {
    $subject = 'Письмо с сайта silagolosakids.ru';
}


if ($post['name'] && $post['phone'] && $post['email']) {

    $name = htmlspecialchars($post['name']);
    $phone = htmlspecialchars($post['phone']);
    $email = htmlspecialchars($post['email']);

    $city = htmlspecialchars($post['city']);
    $msg = htmlspecialchars($post['msg']);

    $message = "Имя: " . $name . "<br>Телефон: " . $phone . "<br>Email: " . $email . "<br>Город: " . $city . "<br>Сообщение: " . $msg;

//		$headers  = "MIME-Version: 1.0\r\n"; 
//		$headers .= "Content-type: text/plain; charset=utf-8\r\n";
//		$headers .= "From: $email\r\n";
//		
//		mail($to,$subject,$message,$headers);

    smtpmail('', 'r2403101@gmail.com', $subject, $message);
    smtpmail('', 'franchise@silaslovakids.ru', $subject, $message);


    //    //добавляем лид в Б24
    $postData = array(
        'fields' => array(
            'TITLE' => $subject,
            'NAME' => $name,
            'COMMENTS' => "Город: " . $city . "<br/>Сообщение: " . $msg,
            'ASSIGNED_BY_ID' => 4,
            'STATUS_ID' => 'NEW',
            'SOURCE_ID' => 1,
            "OPENED" => "Y",
            "EMAIL" => array(
                array(
                    "VALUE" => $email,
                    "VALUE_TYPE" => "WORK"
                )
            ),
            "PHONE" => array(
                array(
                    "VALUE" => $phone,
                    "VALUE_TYPE" => "WORK"
                )
            ),
        ),
        'params' => array(
            "REGISTER_SONET_EVENT" => "Y"
        )
    );


    $queryUrl = 'https://silaslova.bitrix24.ru/rest/1/draro4eg1g5nx4gg/crm.lead.add.json';
    $queryData = http_build_query($postData);
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData
    ));
    $result = curl_exec($curl);
    curl_close($curl);

    $arrResult = json_decode($result, true);


    echo '<p class="green">Ваше сообщение успешно доставлено</p>';
} else {
    echo '<p class="green">Данные заполнены некорректно</p>';
}
?>