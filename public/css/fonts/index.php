<?

function svg($name){

	$txt = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/svg/'.$name.'.svg');

	return '<div class="svg">'.$txt.'</div>';
}

?><!DOCTYPE html>
<html>
    
<head>
<!--<script>

    document.ondragstart = noselect;

    document.onselectstart = noselect;

    document.oncontextmenu = noselect;

    function noselect() {return false;}

</script>-->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '346601732648921');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=346601732648921&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
 
<title>Новый тренд в детском образовании! Курс школы речевых коммуникаций "Сила Слова KIDS"</title>
	<meta charset="UTF-8" />
        <meta name="google-site-verification" content="e6reKCjAb8GxL9UKdgGTVcpcZxHD7egi-xfvTpnhHL4" />
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1, user-scalable=no" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&amp;subset=cyrillic" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="css/style.css" />

	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" media="screen and (max-width: 1023px)" href="css/w768.css" />
	<link rel="stylesheet" type="text/css" media="screen and (max-width: 682px)" href="css/w320.css" />


	<!--<script src="js/jquery-1.11.3.min.js"></script>-->
	<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  
	<script src="js/jquery.formstyler.min.js"></script>
	<script src="js/jquery.maskedinput.js"></script>
	<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
	<script src="js/change-img.js"></script>

	<!-- Fancybox -->
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.min.css" />
	


	<script src="js/script.js"></script>


	<meta property="og:image" content="/images/shared.jpg" />
	<link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32" />
	 <link rel="icon" type="image/png" href="/images/favicon-16x16.png" sizes="16x16" />

</head>

<body>

<div class="dm-overlay" id="win1">
    <div class="dm-table">
        <div class="dm-cell">
            <div class="dm-modal">
                <a href="#close" class="close"></a>
			   <h3 style="font-size: 16px; color: #000;">Получите пошаговый план запуска курса "Юный оратор. Трилогия"</h3><p></p>
			   <form>
			   <div class="messages"></div>
			   	<input style="width: 100%;" type="text" name="name" value="" placeholder="Ваше имя" /><p></p>
				<input style="width: 100%;" name="phone" value="" placeholder="Ваш номер" type="tel" /><p></p>
				<input style="width: 100%;" type="email" name="email" value="" placeholder="Ваша электронная почта" /><p></p>
				<input type="text" name="city" style="width: 100%;" placeholder="Ваш город" /><p></p>
				<input style="width: 100%;" class="zav" type="submit" value="ОСТАВИТЬ ЗАЯВКУ" />
								<p></p><div class="over policy">
					<div class="left"><input style="color: #000;" type="checkbox" name="policy" checked/></div>
					<div class="right" style="color: #000;">Я согласен на обработку моих персональных данных</div>
				</div>
			   </form>
            </div>
        </div>
    </div>
</div>
<div id="backdrop"></div>
<div id="box">
<!-- <img src="../images/target_left.png" width="50" alt=""> -->
	<div id="backdroptext">
		<div id="leftbox"></div>
		<h3>Спасибо за обращение!</h3>
		<div id="rightbox"></div>
		<p>Мы в ближайшее время с Вами свяжемся.</p>
	</div>
<!-- <img src="../images/target_right.png" width="50" alt=""> -->
	<div id="close"></div>
</div>
	<div id="nav">
		<a data-page="1"></a>
		<a data-page="2"></a>
		<a data-page="3"></a>
		<a data-page="4"></a>
		<a data-page="5"></a>
		<a data-page="6"></a>
		<!--a data-page="7"></a-->
		<a data-page="8"></a>
		<a data-page="9"></a>
		<a data-page="10"></a>
		<!--a data-page="11"></a-->
		<!--a data-page="12"></a-->
		<a data-page="13"></a>
		<a data-page="14"></a>
	</div>

	<div id="gwrap">

	<a class="gpage" data-page="1"></a>

	<header class="main">
		<div class="content over">
			<div class="left">
				<h1 class="blank">
					<a href="#">Сила слова KIDS</a>
				</h1>
			</div>
			<div class="right">
				<? echo svg('phone'); ?>
				<div class="txt">
					<!-- <p class="text">Звонок по России бесплатный</p> -->
					<p class="phone"><a href="tel:+79061064684" onclick="yaCounter49872658.reachGoal('telefon_header'); return true;">+7 906 106-46-84</a></p>
				</div>
			</div>
		</div>
	</header>

	<section class="block" id="scream_man">
		<div class="wrapper">
			<video autoplay loop muted>
				<source src="/video/kids.mp4" type="video/mp4"  />
				<source src="/video/kids.webm" type="video/webm" />
			</video>
			<div class="gray">
				<div class="content">
					<div class="note">
						<div class="main"><img class="imgs" src="/images/line3.png"><? //echo svg('1'); ?></div>
						<h2>Запустите курс <br><span class="titles">«Юный оратор.Трилогия»</span></h2>
                        <p><span class="scream"><span class="summ">на базе своего учебного центра</span></span></p>
						<p>и дополнительно зарабатывайте от<span class="scream"><span class="summ xxx">150 000</span> <span class="rubl">₽</span></span> <span class="mobile2">в месяц</span><span class="mobile"><br>в месяц</span></p>
<br>
						<ul class="blank gline">
							<li><strong>Минимальные стартовые вложения</strong></li>
							<li><strong>Исчерпывающее руководство по старту курса</strong></li>
							<li><strong>Высочайший спрос на оказываемые услуги</strong></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<img class="imgs2" src="/images/Group.png">
						<ul class="blank2"><li>- Минимальные стартовые вложения</li><li>- Исчерпывающее руководство по старту курса</li><li>- Высочайший спрос на оказываемые услуги</li>
						</ul>
		
		<? include 'form.php'; ?>
	</section>

<section class="block blocks">
<div class="content">

</div>
</section>
<div class="row row_btn blank2">
				<a class="btn to_callback greens" href="#win1">Получить план запуска&nbsp;&nbsp;&nbsp;<i class="warrow"></i></a>
			</div>

	<section class="block" id="franchise_indicators">
		<div class="content">
			<a class="gpage" data-page="2"></a>
			<h2>Ключевые показатели курса</h2>
			<div class="relative">
				<ul class="row blank franchise_icon">
					<li class="span3">
						<div class="icon"><img src="/images/bl2_coin.png" alt="" /></div>
						<strong>150 000 <span class="rubl">рублей</span></strong> - стартовые инвестиции
					</li>
					<li class="span3">
						<div class="icon"><img src="/images/bl2_home.png" alt="" /></div>
						<strong>2 недели</strong> - запуск курса
					</li>
					<li class="span3">
						<div class="icon"><img src="/images/bl2_time.png" alt="" /></div>
						За <strong>3 месяца</strong> - окупаются инвестиции
					</li>
					<li class="span3">
						<div class="icon"><img src="/images/bl2_money.png" alt="" /></div>

						Прибыль на 4-й месяц<br /><strong>+ 150 000 руб./мес</strong>
					</li>
				</ul>
			</div>

			<div class="row row_btn">
				<a class="btn to_callback greens2" href="#win1">Получить презентацию&nbsp;&nbsp;&nbsp;<i class="warrow"></i></a>
			</div>
		</div>
	</section>

	<section class="block" id="girl_rupor">
		<div class="wr">
			<? echo svg('5'); ?>
			<a class="gpage" data-page="3"></a>
			<h2>Об авторах курса <span>«Юный оратор.Трилогия»</span></h2>
			<div class="right span7 note">
				<h3>Школа Речевых Коммуникаций</h3>
				<p>Школа Речевых Коммуникаций «Сила Слова KIDS» — это образовательный проект для детей и подростков в возрасте 5-16 лет, направленный на развитие навыков красноречия и коммуникаций: преодоление страха публичных выступлений и боязни быть услышанным.</p>
				<p>«Сила Слова KIDS» — единственная сеть школ речевых коммуникаций в России, не имеющая аналогов по качеству обучения и профессионализму. Педагоги и тренеры школы «Сила Слова KIDS» работают по специально разработанным методическим программам с применением инновационных технологий на уроках.</p>
			<img class="imgs5" src="/images/girl_rupor.png">
			</div>
		</div>
	</section>

	<section class="block" id="desc">
		<div class="wr">
			<div class="content">
				<? echo svg('3'); ?>
				<a class="gpage" data-page="4"></a>
				<div class="prev">Немного о продукте</div>
				<h2>Курс «Юный Оратор.Трилогия»</h2>
				<div class="ab">системная программа, рассчитанная на семь месяцев, состоит из 7 шагов:</div>

				<div id="desc_slider" class="over">
					<div class="left span4">
						<ul class="blank super_slider">
							<li data-index="1" class="sl1 active">Техника речи</li>
							<li data-index="2" class="sl2">Актерское мастерство</li>
							<li data-index="3" class="sl3">Вокальная йога</li>
							<li data-index="4" class="sl4">Эффективные коммуникации</li>
							<li data-index="5" class="sl5">Дебаты</li>
							<li data-index="6" class="sl6">Образное мышление</li>
							<li data-index="7" class="sl7">Ораторское искусство</li>
						</ul>
					</div>
					<div class="right span7">
						<div class="jslider">
							<div class="nav">
								<a class="prev"></a>
								<a class="next"></a>
							</div>
							<ul class="blank">
								<li data-index="1"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_1.png" alt="" /></li>
								<li data-index="2"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_2.png" alt="" /></li>
								<li data-index="3"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_3.png" alt="" /></li>
								<li data-index="4"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_4.png" alt="" /></li>
								<li data-index="5"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_5.png" alt="" /></li>
								<li data-index="6"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_6.png" alt="" /></li>
								<li data-index="7"><img class="imgs6" src="/slider/sila_golosa_26_06_18_slider_7.png" alt="" /></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="target">
					<div class="head">Цель курса:</div>
					<div class="txt">красноречивая и уверенная в себе коммуникабельная личность</div><p align="center"><img class="imgs7" src="/images/line2.png"></p>
				</div>

				<a class="gpage" data-page="5"></a>
				<div id="desc_tech" class="over">
					<div class="right span5">
						<h3>Технологичность курса</h3>
						<div class="sub"><strong>Фиджитал технологии</strong> объединение двух реальностей - физической и виртуальной</div>
						<ul class="blank gline">
							<li>Фиджитал технологии (объединение двух реальностей - физической и виртуальной)</li>
							<li>Игровые практики (система вовлечения учеников)</li>
							<li>Геймификация (система поощрения учеников)</li>
							<li>Интерактивная образовательная платформа</li>
							<li>Технология VR (зал публичных выступлений)</li>
							<li>Домашнее задание по авторским тетрадям-практикумам</li>
						</ul>
					</div><img class="imgs5" src="/images/virt_girl.png">
				</div>

				<div id="desc_video" class="span7">
					<div class="head">Результат</div>
					<img class="imgs8" src="/images/redline.png">
					<!-- <a href="/video/silagolosa.mp4" class="fancybox"> -->
					<video class="vids" controls>
 		 			<source src="/video/silagolosa.mp4" type="video/mp4">
					Your browser does not support the video tag.
					</video>
						<!-- <img src="/images/video.jpg" alt="" /> -->
					<!-- </a> -->
				</div>

			</div>
		</div>
	</section>

	<section class="block" id="franchise">
		<div class="content">
			<a class="gpage" data-page="6"></a>
			<h2>Почему «Юный Оратор.Трилогия» — это выгодно?<br /><span>Актуальность курса</span></h2>

			<div id="franchise_notes">
				<div class="txt">
					<ul class="blank">
						<li>В России около <strong>50000</strong> профессий и из них <strong>45000</strong> предполагают умения договариваться, выстраивать цепочки связей, влиять на других людей.</li>
						<li><strong>89% работодателей</strong> считают, что у выпускников средних школ низкие навыки коммуникации (Результаты исследования, проведенного Partnership for 21st Century Skills в России)</li>
						<li><strong>92% родителей беспокоит</strong>, что их дети боятся быть услышанными, не имеют навыков аргументации и убеждения — не умеют формулировать свои мысли и правильно их доносить.</li>
					</ul>
				</div>
				<div class="nav"></div>
			</div>


			<div class="target">На Всемирном экономическом форуме в Давосе в 2017 году навык Эффективных коммуникации - был сформулирован как <strong>№1 в 21 веке</strong>.</div>

			<ul id="franchise_table" class="blank">
				<li class="item1">
					<div class="head">Уникальный продукт</div>
					<div class="txt">«Юный Оратор.Трилогия» - единственный проект в России, который в комплексе успешно закрывает вышеперечисленные задачи. Это детское дополнительное образование нового поколения.</div>
				</li>

				<!--li class="item2">
					<div class="head">Отсутствие сезонности</div>
					<div class="txt">В портфеле школы есть программы, которые успешно реализуются непосредственно в период летних каникул.</div>
				</li-->
				<li class="item3">
					<div class="head">Высочайший спрос</div>
					<div class="txt">на оказываемые услуги более чем 12 заявок в день на открытые уроки (статистика по партнерам сети ШРК "Сила Слова Kids")</div>
				</li>

				<!--li class="item4">
					<div class="head">Инвестиции без обесценивания</div>
					<div class="txt">Инвестиции в рынок детского образования не обесцениваются со временем. Родители вкладывали, вкладывают и будут вкладывать деньги в развитие своих детей.</div>
				</li-->
				<li class="item5">
					<div class="head">Окупаемость</div>
					<div class="txt">Первоначальные инвестиции на запуск курса «Юный Оратор.Трилогия» являются небольшими при рекордном сроке окупаемости — 3-4 месяца.</div>
				</li>
				<li class="item6">
					<div class="head">Актуально</div>
					<div class="txt">Рынок центров развития речи, ораторского мастерства и коммуникации свободен в условиях существования острой проблемы и высокого спроса.</div>
				</li>
			</ul>

			<!--div id="franchise_profit">
				<a class="gpage" data-page="7"></a>
				<h3>Ваши ключевые выгоды с франшизой <span>&laquo;Сила Слова KIDS&raquo;</span></h3>
				<ul class="row blank franchise_icon">
					<li class="span4">
						<div class="icon"><img src="/images/bl2_coin.png" alt="" /></div>
						<div class="head">Гарантия прибыльного открытия</div>
						<div class="txt">Обеспечим наполняемость школы на 80-100% во второй и последующие месяцы с момента открытия.</div>
					</li>
					<li class="span4">
						<div class="icon"><img src="/images/bl2_home.png" alt="" /></div>
						<div class="head">Мощнейшая система маркетинга</div>
						<div class="txt">Мы внедрили самые передовые технологии продвижения. 14 каналов рекламы работают для развития вашего бизнеса.</div>
					</li>
					<li class="span4">
						<div class="icon"><img src="/images/bl2_time.png" alt="" /></div>
						<div class="head">Всесторонняя поддержка &laquo;Общее дело&raquo;</div>
						<div class="txt">Мы не просто продаем франшизу, а ищем полноценных партнеров. Мы не готовы работать с каждым, но со своими франчайзи идем рука об руку.</div>
					</li>
				</ul>
			</div-->

			<div class="row row_btn">
				<a class="btn to_callback reds" href="#win1">Получить фин. модель&nbsp;&nbsp;&nbsp;<i class="warrow"></i></a>
			</div>

		</div>
	</section>
	<? include 'podkluch.php'; ?>
	<section class="block" id="buy_paket">
		<div class="content" style="padding-bottom: 0;">
			<div class="zag">
				<a class="gpage" data-page="9"></a>
				<h2>Финансовые условия</h2>
				<!--<p style="font-size: 23px;">Цена на приобретение лицензии <strong><span>на 1 год</span></strong></FONT></p>-->
				
				
			<div class="icon">Все за вас*</div>
			</div>
			<div class="buy">
				<ul class="row over blank">

					<li class="span0 active">
						<div class="head">Регионы с населением</div>
						<div class="sub sub1">до 700тыс. чел.</div>
						<div class="price1">100 тыс. руб.</div>
						<div class="sub sub2">(паушальный взнос)</div>
						<div class="sep"></div>
						<div class="percent">5%<span class="dohod"> от дохода</span></div>
						<div class="sub sub2">(роялти ежемесячные платежи)</div>
						<!--<div class="price" style="position: relative;top: -49px;color: #f5d200;font-weight: bold;"><span>380</span> тыс.</div>-->
					</li>
					
					<li class="span0 active">
						<div class="head">Регионы с населением</div>
						<div class="sub sub1">от 700тыс. чел.</div>
						<div class="price1">150 тыс. руб.</div>
						<div class="sub sub2">(паушальный взнос)</div>
						<div class="sep"></div>
						<div class="percent">5%<span class="dohod"> от дохода</span></div>
						<div class="sub sub2">(роялти ежемесячные платежи)</div>
						<!--<div class="price" style="position: relative;top: -49px;color: #f5d200;font-weight: bold;"><span>380</span> тыс.</div>-->
					</li>

				</ul>
			</div>
			
			
						<div class="buy">
				<ul class="row over blank">
				
					<li class="span00 active" style=" background: url(../images/bg_fon.png) center center no-repeat; background-size: cover;">
						<div class="sub spec">Специальное предложение!</div>
						<div class="royal">ОТМЕНЯЕМ РОЯЛТИ</div>
						<div class="sub sub2">(ежемесячные платежи)</div>

						<div class="ones">Для первых<span class="two"> 10 центров</span></div>
						<div class="ones">успевших заключить договор</span></div>
						<div class="ones">до<span class="two"> 25 января</span></div>
						<!--<div class="price" style="position: relative;top: -49px;color: #f5d200;font-weight: bold;"><span>380</span> тыс.</div>-->
						<img class="kazah" src="/images/kazah.png">
					</li>
					
					
										<li class="span0 active nopeka" style=" background: url(../images/bg_fon2.png) center center no-repeat; background-size: cover;"><br><br>
						<div class="sub spec">Специальное предложение!</div>
						<div class="royal">ОТМЕНЯЕМ<br>РОЯЛТИ</div>
						<div class="sub sub2">(ежемесячные платежи)</div>
						<img src="/images/penc.png" class="penc">

						<div class="ones">Для первых<span class="two"> 10 центров</span></div>
						<div class="ones">успевших заключить договор</span></div>
						<div class="ones">до<span class="two"> 25 января</span></div>
						<!--<div class="price" style="position: relative;top: -49px;color: #f5d200;font-weight: bold;"><span>380</span> тыс.</div>-->
						<img class="kazah2" src="/images/kazah2.png">
					</li>

				</ul>
			</div>
			
			
									<div class="buy">
				<ul class="row over blank">
				


				</ul>
			</div>
			
			<!--<div class="over note">
				<div class="left span"><h2>Осталось новогодних пакетов:</h2><span class="scream knopka">2</span></div>
			</div>-->
			
			<style type="text/css">
				.to_callback2__text{
					background-color: white;
					padding:20px;
					border-radius: 20px;
					position: relative;
				    font-size: 23 !important;
				    height: fit-content;
				    font-weight: 900;
				    color: #161616;
				    width: 373px;
				    line-height: 1.4;
				}
				.to_callback2__text span{
					color: #ed1921;
				}
				section#worning{
					background-color: #f2f6f7;
					padding-top: 50px;
					padding-bottom: 50px;
				}
				section#worning div.target {
				    box-sizing: border-box;
				    font-size: 25px;
				    font-weight: 900;
				    padding: 35px 95px 47px 95px;
				    background: #fff;
				    border-radius: 10px;
				    box-shadow: 0px 15px 30px 0px rgba(53,53,71,0.1);
				    position: relative;
				}
				section#worning div.target:before {
				    background: url(../images/zagogul_fr_target_l.png) left top no-repeat;
				    left: -59px;
				}
				section#worning div.target:after, section#franchise div.target:before {
				    content: '';
				    position: absolute;
				    width: 88px;
				    height: 171px;
				    top: -16px;
				}
				section#worning div.target strong {
				    font-size: 30px;
				    font-weight: 900;
				    color: #ec1c24;
				}
				section#worning div.target:after {
				    background: url(../images/zagogul_fr_target_r.png) left top no-repeat;
				    right: -59px;
				}
				section#worning div.target:after, section#franchise div.target:before {
				    content: '';
				    position: absolute;
				    width: 88px;
				    height: 171px;
				    top: -16px;
				}
				div.bottom {
				    bottom: -128px;
				}
				div.bottom {
				    bottom: -128px;
				}
				div.bottom {
				    width: 100%;
				    position: absolute;
				    bottom: 0px;
				    z-index: 444;
				}
				.to_callback2{
					flex-direction: row;
					display: flex;
					justify-content: space-around;
				}
				@media screen and (max-width: 1030px) {
					.to_callback2__text {
					    font-size: 18px !important;
					    width: 295px;
					}
					.head{
						margin-bottom: 10px !important;
					}
				}
				@media screen and (max-width: 760px) {
					.to_callback2{
						flex-direction: column;
						justify-content: center;
						align-items: center;
					}
				}

			</style>
			
			<!--<div class="to_callback2">
				<p class="to_callback2__text" style="font-size: 23px;"><span>Специальное предложение:</span></br>  
лицензия выдается на 3 года по цене 1 года для тех, кто заключит договор <span>до 1 декабря !</span></p>
				<img src="images/1.png" style="max-width:100%;">
			</div>-->
			
			<!--div class="scream"><strong>ДО 20 МАЯ</strong> Cкидка на покупку любого тарифа</div-->

			<!--div class="special over" style="position: static;margin-top: 20px;">
				<div class="left">
					<div class="price"><span>Только в ЯНВАРЕ</span></div>скидка на покупку тарифа MAXI
					<div class="price"><span>100 тыс.руб.!</span></div>
				</div>
				<div class="right">
					<a target="_blank" class="btn to_callback">Получить специальное предложение</a>
				</div>
			</div-->
		</div>
	</section>
<script>
$(function() {
  $(".owl-carousel").owlCarousel({
    items: 1,
    merge: true,
    loop: true,
	autoplay: true,
    autoplayTimeout: 3000,
    smartSpeed: 1000,
    margin: 10,
    video: true,

    center: true,
    responsive: {
      320: {
        items: 1
      },
      560: {
        items: 1
      },
      992: {
        items: 1
      }
    }
  });
});

/*
	navText: [
  '<a class="prev1" style="display: block;"></a>',
  '<a class="next1" style="display: block;"></a>'
],
*/
</script>	
		<section class="block">
		<div class="content" style="padding-bottom: 0;">				
					


<div class="home-demo">
  <h2 class="vidass">Отзывы от партнёров</h2>
  <div class="owl-carousel home-slider owl-theme">
    <div class="item-video" data-merge="1">
      <a class="owl-video" href="https://player.vimeo.com/video/374701000"></a>
    </div>
    <div class="item-video" data-merge="1">
      <a class="owl-video" href="https://player.vimeo.com/video/374703045"></a>
    </div>
    <div class="item-video" data-merge="1">
      <a class="owl-video" href="https://player.vimeo.com/video/374701740"></a>
    </div>
  </div>
</div>

 			<div class="row row_btn bts">
				<a class="btn to_callback reds bbs2" href="#win1">Оставить заявку&nbsp;&nbsp;&nbsp;<i class="warrow"></i></a>
			</div>
  </div>
</div>
					
					
		</div>
		</section>

	<section class="block" id="how_open">
		<div class="content">
			<a class="gpage" data-page="10"></a>
			<h2>Как происходит запуск курса</h2>
			<img src="images/zap.png" class="imgs9" style="width: 100%">
			<img src="svg/full2.svg" class="noimgs9" style="width: 100%">
			
		</div>
	</section>

	<section class="block" id="worning">
		<div class="content">
			<h2 style="padding-bottom: 50px;"><span>Важно</span></h2>
			<div class="target"><strong>Но есть одно очень важное условие!</strong>
			Лицензию мы продаем владельцам детских центров или детских садов, то есть людям с опытом работы ведения бизнеса в сфере доп.образования для детей.
			</div>
		</div>
		<style type="text/css">
			.person__with_text{
				display: flex;
				justify-content: center;
				margin-top: 30px;
			}
			p.person__with_text__text{
			    background-color: #3d933c;
			    color: white;
			    padding: 30px;
			    font-size: 23px;
			    font-weight: bold;
			    border-radius: 30px;
			    height: 110px;
			    width: 450px;
			        margin-left: -220px;
			}
			.right__text_side{
				display: flex;
				flex-direction: column;
				justify-content: flex-start;
			}
			.person__with_text__person{
				margin-top: 51px;
			}
			.str_look{
			    transform: scale(0.8);
			    position: relative;
			    top: 24px;
			    left: -44px;
			}
			.over_1_more{
				width: 100%;
			    z-index: 100;
			}
			.overflow_person{
				width: 1298px;
    			margin: 0px auto;
			}
		</style>
		<div class="person__with_text">
			<img src="images/boy.png" alt="" title="" class="person__with_text__person">
			<div class="right__text_side">
				<p class="person__with_text__text">Если чувствуете, что в теме, звоните нам, ведь количество лицензий на регион ограничено в целях сохранения Вашей же рентабельности и прибыли<img src="images/str.png" class="str_look"></p>	
				<img src="images/tuchka.png">	
			</div>
			
			<div class="row row_btn bts">
				<a class="btn to_callback reds bbs" href="#win1">Получить презентацию&nbsp;&nbsp;&nbsp;<i class="warrow"></i></a>
			</div>
			
		</div>
		<div class="over_1_more">
			<div class="overflow_person">
				<div class="send">
					<a href="/images/sertificat.jpg" class="link fancybox"><img src="/images/bottom_link.png" alt=""></a>
					<p style="display: none;">Получите пошаговый план запуска курса "Юный оратор.Трилогия"</p>
					<div class="messages"></div>
					<form class="row">
						<input class="span2" type="text" name="name" value="" placeholder="Ваше имя">
						<input class="span2" type="text" name="phone" value="" placeholder="Ваш телефон">
						<input class="span2" type="text" name="email" value="" placeholder="Ваш  Email">
						<span class="span2 select"><!--<select class="" name="city">
	<option value="">Ваш город</option>
	<option value="Москва">Москва</option>
	<option value="Санкт-Петербург">Санкт-Петербург</option>
	<option value="Казань">Казань</option>
</select>-->
<input type="text" name="city" class="span2" placeholder="Ваш город"></span>
						<span class="span4 btn">
							<input class="" type="submit" value="Получить презентацию">
							<div class="over policy">
								<div class="left"><div class="jq-checkbox checked" unselectable="on" style="user-select: none; display: inline-block; position: relative; overflow: hidden;"><input type="checkbox" name="policy" checked="" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;"><div class="jq-checkbox__div"></div></div></div>
								<div class="right">Я согласен на обработку моих персональных данных</div>
							</div>
						</span>
					</form>
				</div>
			</div>
		</div>
	</section>


	<section class="block" id="gallery">
		<div class="wr">
			<div id="gal_wr">
				<div class="content">
					<a class="gpage" data-page="13"></a>
					<h2>Так могут проходит уроки в <span>Вашем</span> центре</h2>
					<div class="row gallery_list">
						<div class="span6 big">
							<a href="/gallery/full/b1.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #1" ><img src="/gallery/big1.jpg" alt="" /></a>
						</div>
						<div class="span6 small">
							<div class="row">
								<div class="span3">
									<a href="/gallery/full/s1.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #2"><img src="/gallery/small1.jpg" alt=""></a><br><br>
<a href="/gallery/full/s3.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинка #4"><img src="/gallery/small3.jpg" alt=""></a>
								</div>
								<div class="span3">
									<a href="/gallery/full/s2.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #3"><img src="/gallery/small2.jpg" alt="" /></a>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row gallery_list">
						<div class="span6 small">
							<div class="row">
								<div class="span3">
									<a href="/gallery/full/s5.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #6"><img src="/gallery/small5.jpg" alt="" /></a>
								</div>
								<div class="span3">
									<a href="/gallery/full/s6.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #7"><img src="/gallery/small6.jpg" alt="" /></a>
								</div>
							</div>
							<div class="row">
								<div class="span3">
									<a href="/gallery/full/s7.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #8"><img src="/gallery/small7.jpg" alt="" /></a>
								</div>
								<div class="span3">
									<a href="/gallery/full/s8.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #9"><img src="/gallery/small8.jpg" alt="" /></a>
								</div>
							</div>
						</div>
						<div class="span6 big">
							<a href="/gallery/full/b2.jpg" class="fancybox ddd" data-fancybox="gallery" data-caption="Картинки #9"><img src="/gallery/big2.jpg" alt="" /></a>
						</div>
					</div>
				</div>
				<div class="last_zagogul"></div>
			</div>
		</div>
	</section>

	<section class="block" id="callback">
		<div class="content">
			<div class="girl"></div>
			<form class="span6">
				<div class="txt">
					<a class="gpage" data-page="14"></a>
					<h3>Остались вопросы?</h3>
					Расскажем все по телефону или в письме
				</div>
				<div class="messages"></div>
				<input type="text" name="name" value="" placeholder="Ваше имя" />
				<input name="phone" value="" placeholder="Ваш номер" type="tel" />
				<input type="email" name="email" value="" placeholder="Ваша электронная почта" />
				<!--<textarea name="msg" placeholder="Ваш вопрос"></textarea>-->
				<? include 'city.php'; ?>
				<input type="submit" value="Оставить заявку" />
				<div class="over policy">
					<div class="left"><input type="checkbox" name="policy" checked/></div>
					<div class="right">Я согласен на обработку моих персональных данных</div>
				</div>
			</form>
		</div>
	</section>

	<footer class="main">
		<div class="content">
			<h2>Мы всегда на связи</h2>
			<div id="foot_info" class="over">
				<div class="row span7 left">
					<a href="tel:+79061064684" class="span2 fsocial fsocial_phone" onclick="yaCounter49872658.reachGoal('telefon_footer');return true;">+79061064684</a>
					<a href="whatsapp://send?phone=+79061064684" class="span2 fsocial fsocial_whatsapp">+79061064684</a>
					<!--a href="https://vk.com/franchise_silaslovakids" class="span2 fsocial fsocial_vk">vk.com/franchise_silaslovakids</a-->
				</div>
				<!--div class="right">Разработано в <a href="https://imcompany.pro/">ImCompany</a></div-->
			</div>
		</div>
	</footer>

	</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(53233975, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/53233975" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    // код для мобильных устройств
	console.log('mobile');
	$('header.main div.left h1 a').removeAttr('href');
  }

</script>





</body>
</html>