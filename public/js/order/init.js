function change_order_status() {
    $('#order_status_change').change(function () {
        var status = $(this).val();
        var order_id = $('#order_id').text();
        $.ajax({
            url: '/admin/order/changeStatus',
            data: {
                status: status,
                order_id: order_id
            },
            success: function (resp) {
                if (resp === 'ok') {
                    window.location.reload()
                }
                else {
                    alert('Ошибка сохранения статуса')
                }
            }
        })
    });
}

$(document).ready(function () {
    change_order_status();
});