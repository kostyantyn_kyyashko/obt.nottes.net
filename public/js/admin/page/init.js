function savePage()
{
    $('#pageSave').click(function () {
        var pageContent = $('#summernote').summernote('code');
        var slug = $('#pageSlug').val()||$('#pageSlug').text();
        if (!slug) {
            alert('Введите адрес страницы на сайте');
            return;
        }
        if (slug.match('[^a-z0-9_\-]')) {
            alert('В адресе страницы можно использовать только маленькие английские буквы и цифры');
            return;
        }
        $.ajax({
            url: '/admin/page/save',
            type: 'post',
            data: {
                slug: slug,
                pageContent: pageContent
            },
            success: function (resp) {
                console.log(resp);
                if (resp == 'ok') alert('Успешно сохранено')
            }
        })
    })
}

function remove_page_confirm() {
    $('a.remove_page').click(function () {
        if (confirm('Точно удалить?')) return true;
        return false;
    })
}

$(document).ready(function() {
    remove_page_confirm();
    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ],
        lang: 'ru-RU',
        height: 500
    });
    setTimeout(function () {
        savePage();
    }, 1000)
    //$('#summernote').summernote('code','');
});
