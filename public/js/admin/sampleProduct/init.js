function addField() {
    $('#addField').click(function () {
        $.ajax({
            url: '/admin/sampleGood/addField',
            success: function (resp) {
                var data = JSON.parse(resp);
                if (data.html) {
                    $('#fieldsTbody').append(data.html);
                }
            }
        })
    });
}

function saveProductSample() {
    $('#saveProductSample').click(function () {
        var fields = $('.fieldItem');
        var data = [];
        $.each(fields, function (index, field) {
            data.push({
                categoryId: $('#category').val(),//category ID
                categoryName: $('#category option:selected').text(),//category ID
                fieldName: $(field).find('[name=fieldName]').val(),
                dataType: $(field).find('[name=dataType]').val(),
                inFilter: $(field).find('[name=inFilter]').val()
            })
        });
        data = JSON.stringify(data);
        $.ajax({
            url: '/admin/sampleProduct/save',
            type: 'post',
            data: {
                fields: data
            },
            dataType: 'text',
            success: function (resp) {
                if (resp === 'ok') {
                    alert('Успешно сохранено')
                }
                else {
                    alert('Ошибка сохранения')
                }
            }
        })
    })
}

$(document).ready(function () {
    addField();
    saveProductSample();
});