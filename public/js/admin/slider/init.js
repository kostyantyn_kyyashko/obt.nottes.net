function uploadSliderFile() {
    $('#submitSliderData').on('click', function() {
        var form = $('#sliderAddItem')[0];
        var form_data = new FormData(form);
        //alert(form_data);
        $.ajax({
            url: '/admin/slider/upload', // point to server-side PHP script
            type: 'post',
            dataType: 'json',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(resp){
                if (resp.status == 'ok') {
                    $('#slider_img').html('<img  style="width: 500px;" src="' + resp.file + '">');
                    alert('Слайдер успешно сохранен')
                }
            }
        });
        return false;
    });
}

function removeSliderAlert() {
    $('.removeSlider').click(function () {
        if (confirm('Внимание! Этот элемент слайдера будет удален')) {
            return true;
        }
        return false;
    })
}

$(document).ready(function () {
    bsCustomFileInput.init();
    uploadSliderFile();
    removeSliderAlert();
});