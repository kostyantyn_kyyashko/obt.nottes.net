function categorySave() {
    $('#categorySave').click(function () {
        var categoryLevel = $('#categoryLevel').val();
        var categoryParent = $('#categoryParent').val();
        var categoryName = $('#categoryName').val();
        $.ajax({
            url: '/admin/category/save',
            type: 'post',
            data: {
                categoryLevel: categoryLevel,
                categoryParent: categoryParent,
                categoryName: categoryName,
            },
            success: function (resp) {
                if (resp == 'ok') {
                    alert('Категория добавлена успешно')
                }
                else {
                    alert('Ошибка добавления категории. Возможно, такое название категории уже существует')
                }
            }
        })
    })
}

function addParents() {
    $('#categoryLevel').change(function () {
        var categoryLevel = $(this).val();
        $.ajax({
            url: '/admin/category/getByLevel',
            data: {
                categoryLevel: categoryLevel,
            },
            success: function (resp) {
                $('#categoryParent').html(resp)
            }
        })
    })
}

function saveCategoryChanges() {
    $('#saveCategoryChanges').click(function (e) {
        e.preventDefault();
        var categoryName = $('#categoryName').val();
        var categoryWeight = $('#categoryWeight').val();
        var _id = $('#_id').val();
        $.ajax({
            url: '/admin/category/saveChanges',
            data: {
                categoryName: categoryName,
                categoryWeight: categoryWeight,
                _id: _id
            },
            success: function (resp) {
                if (resp == 'ok') {
                    alert('Успешно сохранено2');
                    return false;
                    //window.location.href = '/admin/category/getTree'
                }
                else {
                    alert('Ошибка сохранения. Возможно, категория с таким именем существует');
                    return false;
                }
            }
        })
    })
}

function deleteCategory() {
    $('#removeCategory').click(function () {
        if (confirm('Внимание! Удаление категории приведет к исчезновению подкатегорий\n' +
            ' и товаров этой категории из каталога')) {
            var _id = $('#_id').val();
            $.ajax({
                url: '/admin/category/delete',
                data: {
                    _id: _id
                },
                success: function (resp) {
                    if (resp == 'ok') {
                        alert('Категория удалена');
                        window.location.href = '/admin/category/getTree'
                    }
                    else {
                        alert('Ошибка сохранения. Возможно, категория с таким именем существует')
                    }
                }
            })
        }
    })
}

function uploadCategoryFile() {
    $('#customFileLangHTML').on('change', function() {
        var form = $('#categoryData')[0];
        var form_data = new FormData(form);
        form_data.append('_id', $('#_id').val());
        $.ajax({
            url: '/admin/category/upload',
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(resp){
                if (resp.result === 'ok') {
                    $('#categoryImage').html('<img src="' + resp.file + '">');
                    alert('Изображение успешно сохранено')
                }
                else {
                    alert('Ошибка загрузки файла изображения');
                }
            }
        });
        return false;
    });
}



$(document).ready(function () {
    categorySave();
    addParents();
    saveCategoryChanges();
    deleteCategory();
    uploadCategoryFile();
});