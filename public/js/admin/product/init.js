function goToPage() {
    $('#goToPage').click(function () {
        var page = $('#userPageNum').val();
        if (!page) {
            alert('Введите номер страницы');
            return;
        }
        window.location.href = '?page=' + page;
    })
}

function modalCategoryName() {
    $('#treeContentModal').find('a').click(function () {
        var text = $(this).text();
        var categoryHref = $(this).attr('href');
        $.ajax({
            url: '/admin/product/getRootCategory',
            data: {
                categoryHref: categoryHref
            },
            success: function (resp) {
                $('#rootCategory').val(resp);
            }
        });
        $('#category').val(text);
        $('a.close-modal').click();
        return false;
    })
}

function uploadProductFile() {
    $('#customFileLangHTML').on('change', function() {
        var form = $('#productImageForm')[0];
        var form_data = new FormData(form);
        $.ajax({
            url: '/admin/product/upload',
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(resp){
                if (resp.result === 'ok') {
                    $('#productImages')
                        .append('<img title="Двойной клик для удаления" class="productImage" src="' + resp.file + '">');
                    alert('Изображение успешно добавлено');
                    removeImage();
                }
                else {
                    alert('Ошибка загрузки файла изображения');
                }
            }
        });
        return false;
    });
}

function removeImage() {
    $('.productImage').unbind();
    $('.productImage').dblclick(function () {
        $(this).remove();
    })
}

function saveProduct() {
    $('#saveProduct').click(function () {
        var productId = $('#productId').val(),
            name = $('#name').val(),
            category = $('#category').val(),
            rootCategory = $('#rootCategory').val(),
            price = $('#price').val(),
            description = $('textarea#description').val(),
            files = [];
        var realImg = $('.productImage');
        $.each(realImg, function (index, file) {
            var src = $(file).attr('src');
            files.push(src)
        });
        $.ajax({
            url: '/admin/product/save',
            type: 'post',
            data: {
                productId: productId,
                name: name,
                category: category,
                rootCategory: rootCategory,
                price: price,
                description: description,
                files: JSON.stringify(files)
            },
            success: function (resp) {
                if (resp === 'ok') {
                    var fields = [];
                    var fieldRow = $('.addFieldRow');
                    $.each(fieldRow, function (index, row) {
                        var rowObj = {};
                        rowObj.productId = productId;
                        rowObj.rootCategory = rootCategory;
                        rowObj.fieldName = $(row).find('input.field-name').val();
                        rowObj.fieldValue = $(row).find('input.field-value').val();
                        rowObj.fieldDataType = $(row).find('.field-dataType').val();
                        rowObj.fieldItem = $(row).find('input.field-item').val();
                        fields.push(rowObj);
                    });
                    console.log(fields);
                    $.ajax({
                        url: '/admin/product/saveFields',
                        data: {
                            fields: JSON.stringify(fields)
                        },
                        success: function (resp) {
                            if (resp === 'ok') {
                                alert('Успешно сохранено');
                            }
                            else {
                                alert('Ошибка сохранения полей');
                            }
                        }
                    });
                }
                else {
                    alert('Ошибка сохранения данных');
                }
            }
        });
    })
}

function addField() {
    $('#addField').click(function () {
        $.ajax({
            url: '/admin/product/addField',
            success: function (resp) {
                $('#fields').append(resp);
                removeField();
            }
        })
    })
}

function removeField() {
    $('.removeField').unbind();
    $('.removeField').click(function () {
        $(this).parent().parent().remove();
    })
}

$(document).ready(function () {
    goToPage();
    modalCategoryName();
    uploadProductFile();
    saveProduct();
    removeImage();
    addField();
    removeField();
});