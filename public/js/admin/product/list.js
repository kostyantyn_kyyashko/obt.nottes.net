function modalCategoryName() {
    $('#treeContentModal').find('a').click(function () {
        var categoryChild = $(this).text();
        var categoryHref = $(this).attr('href');
        $.ajax({
            url: '/admin/product/getRootCategory',
            data: {
                categoryHref: categoryHref
            },
            success: function (resp) {
                var rootCategory = resp;
                $.each($('.cb:checked'), function (index, cb) {
                    $(cb).parent().parent().find('.rootCategory').text(rootCategory);
                    $(cb).parent().parent().find('.category').text(categoryChild);
                    var productId = $(cb).parent().parent().attr('id');
                    $.ajax({
                        url: '/admin/product/updateCategories',
                        data: {
                            productId: productId,
                            category: categoryChild,
                            rootCategory: rootCategory
                        },
                        success: function (resp) {
                            if (resp != 'ok') alert('Ошибка сохранения')
                        }
                    })
            })
            }
        });
        $('a.close-modal').click();
        return false;
    })
}

function check_all_cb() {
    $('#check_all').click(function () {
        var value = $(this).prop('checked');
        $('.cb').prop('checked', value);
    })
}

$(document).ready(function () {
    modalCategoryName();
    check_all_cb();
});