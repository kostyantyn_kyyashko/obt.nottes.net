ymaps.ready(function () {
    var myMapContact = new ymaps.Map('map_contact', {
        center: [53.24490651671663, 44.96455649999989],
        zoom: 15
      }, {
        searchControlProvider: 'yandex#search'
      }),
      myPlacemarkContact = new ymaps.Placemark([53.24490651671663, 44.96455649999989], {
        hintContent: 'г. Пенза, 631 км. трассы М5,  м-н "Обтекатель"',
        balloonContent: 'г. Пенза, 631 км. трассы М5,  м-н "Обтекатель"'
      }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/img/map-ballun.png',
        // Размеры метки.
        iconImageSize: [74, 75],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-5, -38]
      });

  myMapContact.geoObjects
  .add(myPlacemarkContact)
})