$(document).ready(function () {
  $("body").on("click", ".cmn-toggle-switch__htx", function (event) {
    $('.cmn-toggle-switch__htx').toggleClass('active');
  });

  $("body").on("click", ".modal-call", function () {
    var thisAttr = $(this).attr('data-value');
    console.log(thisAttr);
    $('.remodal .hidden-value').attr('value', thisAttr);
  });

  $("body").on("click", ".cmn-toggle-switch", function (event) {
    $('.top-menu').toggleClass('active');
    $('body').toggleClass('overf');
  });

  $("body").on("click", "nav.active ul li a", function (event) {
    $('.top-menu').toggleClass('active');
    $('.cmn-toggle-switch__htx').toggleClass('active');
    //  $('body').removeClass('overf');
  });
  $("body").on("click", ".mobile-catalog-toggle", function (event) {
    $(this).toggleClass('active');
    $('.mobile-catalog-menu > ul').toggleClass('active');
  });
  $("body").on("click", ".mobile-catalog-menu > ul > li", function (event) {
    $(this).toggleClass('active');
  });
  $("body").on("click", ".card", function (event) {
    $('.card-mobile').toggleClass('active');

  });
  $("body").on("click", ".card-mobile__closer", function (event) {
    $('.card-mobile').toggleClass('active');

  });
  $("body").on("click", ".mobile-param-filter", function (event) {
    $('.sidebar-param-filter').toggleClass('active');
  });

  $("body").on("click", ".order-1-step #btn-select-pay", function () {
    $('.container-pay').show();
    $('.order-1-step').hide();
    $('.order-2-step').css('display','flex');
  });
  $("body").on("click", ".order-2-step #btn-select-deverly", function () {
    $('.container-deverly').show();
    $('.order-2-step').hide();
    $('.order-3-step').css('display','flex');
  });
  $("body").on("click", ".order-3-step #btn-succes-order", function () {
    $('.container-end').show();
    $('.order-3-step').hide();
    $('#order-complite').removeClass('disabled')
  });


  $("body").on("click", " .param-filter__row__head", function () {
    $(this).find('.param-filter__head__toggler').toggleClass('close');
    $(this).next().slideToggle();
  });

});




$(document).ready(function () {
  $('label.checkbox-label input').click(function () {
    $(this).parent().toggleClass('active');
  });
  $('.label-select-pay label.radio-label input').click(function () {
    $('.label-select-pay .radio-label').removeClass('active');
    $(this).parent().addClass('active');
  });
  $('.label-select-deverly label.radio-label input').click(function () {
    $('.label-select-deverly .radio-label').removeClass('active');
    $(this).parent().addClass('active');
  });
  $('label.bx_filter_param_label input').click(function () {
    $(this).parent().parent().toggleClass('active');
  })
});



$(document).on('ready', function () {
  $(".regular").slick({
    dots: false,
    arrows: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 769,
        settings: {
          dots: true,
          arrows: false,

          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          dots: true,
          slidesToShow: 1
        }
      }
    ]

  });
  $(".bsi-slider").slick({
    dots: false,
    arrows: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 768,
        settings: {
          dots: true,
          arrows: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 590,
        settings: {
          dots: true,
          arrows: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          dots: true,
          slidesToShow: 2
        }
      }
    ]

  });
});


ymaps.ready(function () {
  var myMap = new ymaps.Map('map', {
    center: [53.24505453489378, 44.97178773550403],
    zoom: 15
  }, {
    searchControlProvider: 'yandex#search'
  });

 


    myPlacemark = new ymaps.Placemark([53.24490651671663, 44.96455649999989], {
      hintContent: 'г. Пенза, 631 км. трассы М5,  м-н "Обтекатель"',
      balloonContent: 'г. Пенза, 631 км. трассы М5,  м-н "Обтекатель"'
    }, {
      // Опции.
      // Необходимо указать данный тип макета.
      iconLayout: 'default#image',
      // Своё изображение иконки метки.
      iconImageHref: '/img/map-ballun.png',
      // Размеры метки.
      iconImageSize: [74, 75],
      // Смещение левого верхнего угла иконки относительно
      // её "ножки" (точки привязки).
      iconImageOffset: [-5, -38]
    });





  myMap.geoObjects
    .add(myPlacemark)



});

$(function () {
  $('.img-prev').click(function () {
    var src = $(this).children('img').eq(0).attr('src');
    $('#img img').attr('src', src);
  })
})


$(function () {

  $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
    $(this).addClass('active').siblings().removeClass('active')
      .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
  })

})

//Код jQuery, установливающий маску для ввода телефона элементу input
//1. После загрузки страницы,  когда все элементы будут доступны выполнить...
$(function(){
  //2. Получить элемент, к которому необходимо добавить маску
  $('input[name="phone"]').mask("9(999) 999-9999");
});
/*-------------------------------------------------------------------------------------------------*/

function imgZoom() {
  $('#img-main').ezPlus({
    tint:true,
    preloading: 0
  });
  $('.img-prev').click(function (){
    var html = $(this).html();
    var img = $(html).attr('id', 'img-main');
    $('#img').html('');
    $('#img').append(img);
    $('#img-main').ezPlus({
      tint:true,
      preloading: 0
    });
  })

}

function add_to_card() {
  $('#buy_button, .buy-btn-catalog').click(function () {
    var product_id = $(this).attr('href'),
        card = $.cookie('card'),
        product_count = $('#product_count').val();
        if (typeof product_count === 'undefined') {
          product_count = $(this).parent().parent().find('.quantity-num').val()
        }
    if (typeof card != 'undefined') {
      card = JSON.parse(card);
    }
    else {
      card = [];
    }
    card.push({
      product_id: product_id,
      product_count: product_count
    });
    card = JSON.stringify(card);
    $.cookie('card', card, { expires: 7, path: '/' });
    setTimeout(function () {
      update_card();
      alert('Успешно добавлено в корзину');
    }, 200);
    return false;
  });
}

function update_card() {
  var count = $.cookie('card');
  if (typeof count == 'undefined' || !count) {
    $('#card__text').text('Ваша корзина пуста')
  }
  else {
    count = JSON.parse(count);
    count = count.length;
    $('#card__text').html('В корзине позиций: <b>' + count + '</b>')
  }
}

function update_item_price() {
  $('.catd-list-qwt').find('input').change(function () {
    var root = $(this).parent().parent();
    var new_count = $(this).val(),
        price = $(root).find('.item_price').text();
    price = parseInt(price);
    var new_total_price = price*new_count;
    $(root).find('.item_total_price').text(new_total_price);
    update_total_price();
    update_card_cookie();
  })
}

function update_total_price() {
  var items = $('.item_total_price'),
      total_price = 0;
  $.each(items, function (index, item_price) {
    total_price += parseInt($(item_price).text())
  });
  $('#card_total_price').text(total_price);
}

function remove_card_item() {
  $('.card-list-delete').click(function () {
    var item_name = $(this).parent().parent().find('.item_name').text();
    if(confirm('Удалить товар\n ' + item_name + '\n из корзины?'))
    $(this).parent().parent().remove();
    update_total_price();
    update_card_cookie();
    return false;
  })
}

function update_card_cookie() {
  var items = $('.card-body'),
      total = [];
  $.each(items, function (index, item) {
    var data = {
      product_id: $(item).find('.product_id').val(),
      product_count: $(item).find('.product_count').val()
    };
    total.push(data);
  });
  total = JSON.stringify(total);
  $.cookie('card', total);
  setTimeout(function () {
    update_card();
  }, 200);
}

function create_order() {
  $('#btn-succes-order').click(function () {
    var order_content = $.cookie('card'),
        phone = $('#order-phone').val(),
        fio = $('#order-fio').val(),
        email = $('#order-email').val(),
        city = $('#order-city').val(),
        street = $('#order-street').val(),
        house = $('#order-house').val(),
        flat = $('#order-flat').val(),
        delivery_type = $('[name=select-deverly]:checked').attr('data-type'),
        pay_type = $('[name=select-pay]:checked').attr('data-type'),
        total_price = parseInt($('#total_price').text()),
        delivery_cost = parseInt($('#delivery_cost').text()),
        whole_price = parseInt($('#whole_price').text());
    var check_fio = check_input('order-fio'),
        check_phone = check_input('order-phone'),
        check_email = check_input('order-email');
    var data = {
      fio: fio,
      phone: phone,
      email: email,
      city: city,
      street: street,
      house: house,
      flat: flat,
      delivery_type: delivery_type,
      pay_type: pay_type,
      total_price: total_price,
      delivery_cost: delivery_cost,
      whole_price: whole_price,
      order_content: order_content,
    };
    data = JSON.stringify(data);
    if (check_fio && check_email && check_phone) {
      $.ajax({
        url: '/createOrder',
        data: {
          data: data,
          order_content: order_content
        },
        dataType: 'json',
        success: function (resp) {
          if (resp.status === 'ok') {
            window.location.href='/orderSuccess?order_id=' + resp.orderId
          }
          else {
            alert('Ошибка создания заказа');
          }
        }
      })
    }
    else {
      alert('Заполните необходимые поля')
    }
  })
}

function check_input(input_id) {
  var input = $('#' + input_id);
  if (!input.val()) {
    input.css({border: '1px solid #ff0000'});
    return false;
  }
  else {
    input.css({border: '1px solid #ebebeb'});
    return true;
  }
}

function change_delivery_price() {
  $('[name=select-deverly]').change(function () {
    var delivery_cost = $(this).attr('data-price');
    $('#delivery_cost').text(delivery_cost);
    $('#whole_price').text(parseInt(delivery_cost) + parseInt($('#total_price').text()));
  })
}

function check_phone() {
  $('#order-phone').change(function () {
    var phone = $(this).val();
    phone = phone.replace(/[^\d]/g, '');
    //phone = '7' + phone.substr(1);
    $(this).val(phone);
    $.ajax({
      url: '/order/get_user_by_phone',
      data: {
        phone: phone
      },
      dataType: 'json',
      success: function (resp) {
        if (resp.status === 'ok') {
          $('#order-fio').val(resp.fio);
          $('#order-email').val(resp.email);
          $('#order-city').val(resp.city);
          $('#order-street').val(resp.street);
          $('#order-house').val(resp.house);
          $('#order-flat').val(resp.flat);
        }
      }
    })
  })
}

function send_login_SMS() {
  $('#phone_login_submit').click(function () {
    var phone = $('#phone_login').val();
    phone = phone.replace(/[^\d]/g, '');
    phone = '7' + phone.substr(1);
    $('#phone_login').val(phone);
    $.ajax({
      url: '/sendSMS',
      data: {
        phone: phone
      },
      success: function (resp) {
        if (resp === 'fail') {
          alert('Пользователь с таким телефоном не зарегистрирован')
        }
        else {
          $.cookie('sms', resp);
          $.cookie('phone', phone);
        }
      }
    })
  })
}

function check_login_SMS() {
  $('#phone_login_check').click(function () {
    var value = $('#phone_value').val();
    var hash = $.cookie('sms');
    $.ajax({
      url: '/checkSMS',
      data: {
        value: value,
        hash: hash
      },
      success: function (resp) {
        SMS_callback(resp);
      }
    })
    $('a.close-modal').click()
  })
}

function SMS_callback(resp) {
  if (resp === 'ok') {
    $.ajax({
      url: '/checkUser',
      dataType: 'json',
      data: {
        phone: $.cookie('phone')
      },
      success: function (resp) {
        $.removeCookie('sms');
        $.removeCookie('phone');
        $.cookie('userId', resp.userId, { expires: 365, path: '/' });
        alert('Рады приветствовать, ' + resp.fio);
        window.location.reload();
      }
    })
  }
  else {
    alert('Авторизация отклонена')
  }
}

function sort_order() {
  $('#order').change(function () {
    var order = $(this).val();
    var url = window.location.href;
    if (order) {
      if (url.indexOf('order=asc') > 0) {
        url = url.replace('order=asc', 'order=desc')
      }
      else {
        if (url.indexOf('order=desc') > 0) {
          url = url.replace('order=desc', 'order=asc')
        }
        else {
          if(url.indexOf('?') > 0){
            url = url + '&order=asc'
          }
          else {
            url = url + '?order=asc'
          }
        }
      }
    }
    else {
      url = url.replace(/order=\w{3,4}/, '');
    }
    window.location.href = url;
  })
}

function validate_phone() {
  $('#order-phone').change(function () {
    var phone = $(this).val();
    phone = phone.replace(/[^\d]/g, '');
    phone = '7' + phone.substr(1);
    $(this).val(phone)
  })
}

function update_user() {
  $('#update_user').click(function () {
    var user_data = {
      fio: $('#fio').val(),
      email: $('#email').val(),
      city: $('#city').val(),
      street: $('#street').val(),
      house: $('#house').val(),
      flat: $('#flat').val(),
    };
    var phone = $('#phone').val();
    $.ajax({
      url: '/updateUser',
      data: {
        user_data: user_data,
        phone: phone
      },
      success: function (resp) {
        if (resp === 'ok') {
          alert('Успешно сохранено')
        }
        else {
          alert('Ошибка сохранения')
        }
      }
    })
    return false;
  })
}

function logout_user() {
  $('#logout_user').click(function () {
    $.removeCookie('userId');
    window.location.href = '/';
    return false;
  })
}

function feedback_send() {
  $('#feedback-form').submit(function () {
    var name = $('[name=name]').val(),
        phone = $('[name=phone]').val();
    $.ajax({
      url: '/sendFeedback',
      data: {
        name: name,
        phone: phone
      },
      success: function (resp) {
        if (resp == 'ok') {
          alert('Ваше сообщение успешно отправлено')
        }
        else {
          alert('Ошибка отправки сообщения')
        }
      }
    });
    return false;
  })
}

$(document).ready(function () {
  //imgZoom();
  update_card();
  add_to_card();
  update_item_price();
  remove_card_item();
  create_order();
  change_delivery_price();
  check_phone();
  send_login_SMS();
  check_login_SMS();
  sort_order();
  validate_phone();
  update_user();
  logout_user();
  feedback_send();
});

$(function() {

  (function quantityProducts() {
    var $quantityArrowMinus = $(".quantity-arrow-minus");
    var $quantityArrowPlus = $(".quantity-arrow-plus");

    $quantityArrowMinus.click(quantityMinus);
    $quantityArrowPlus.click(quantityPlus);

    function quantityMinus() {
      if ($(this).next().val() > 1) {
        $(this).next().val(+$(this).next().val() - 1);
      }
    }

    function quantityPlus() {
      $(this).prev().val(+$(this).prev().val() + 1);
    }
  })();

});

(function($) {
  $(function() {

    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
      $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });

  });
})(jQuery);